\documentclass[]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian, english]{babel}


\usepackage{csvsimple}

\usepackage{xspace}
\usepackage{hyperref}
\usepackage{booktabs}
\usepackage{caption}
\captionsetup{tableposition=top,figureposition=bottom,font=small,labelfont={sc,bf},format=plain}

\RequirePackage{amsmath,amssymb}  % Pacchetto per l'inserimento delle formule matematiche
%\RequirePackage{tikz} % pacchetto per fare grafici e disegni
\usepackage{graphicx}

\usepackage{siunitx}
\sisetup{
	round-mode=places,
}


\usepackage{cleveref}


\usepackage[autostyle,italian=guillemets]{csquotes}
\usepackage[citestyle=numeric-comp, babel=hyphen, sortsets=true, backref=true, backend=biber]{biblatex}
\addbibresource{biblio.bib}


\newcommand{\clark}{\textsc{CLARK}\xspace}
\newcommand{\clarks}{\textsc{CLARK-\emph{S}}\xspace}
\newcommand{\missh}{\textsc{MISSH}\xspace}
\newcommand{\MISSH}{\missh}
\newcommand{\cpp}{{C\nolinebreak\hspace{-.05em}\raisebox{.4ex}{\tiny\bf +}\nolinebreak\hspace{-.10em}\raisebox{.4ex}{\tiny\bf +}}\xspace}
\newcommand{\srr}{SRR1804065\xspace}

\newcommand{\qt}[1]{\textquotedblleft#1\textquotedblright\xspace}
\newcommand{\QT}[1]{«#1»\xspace}

\newcommand{\gseq}[1]{{\fontfamily{ppl}\fontseries{sbx}\selectfont{#1}}}
\newcommand{\Ab}{\gseq{A}\xspace}
\newcommand{\Cb}{\gseq{C}\xspace}
\newcommand{\Gb}{\gseq{G}\xspace}
\newcommand{\Nb}{\gseq{N}\xspace}
\newcommand{\Tb}{\gseq{T}\xspace}




%opening
\title{Time performance of \missh within \clarks}
\author{Giorgio Gallina}

\begin{document}

\maketitle

%\begin{abstract}
%
%\end{abstract}

\section{Introduction}

Classification is a very relevant task in bioinformatics. In this field, \clark is a software for genomic and metagenomic binning, as presented in~\cite{clark:clark}. It carries out such a task by comparing the sequences to be classified, which we call \emph{objects}, with a set of reference sequences, called \emph{targets}. The comparison is made by means of a \emph{hash table of discriminative target k-mers}, where discriminative simply means that k-mers which are not exclusive of one single target are ignored. The objects assignment is made by comparing the k-mers of an object with the target k-mers.

A more sophisticate strategy is to allow for some (fixed in position) mismatches in the k-mers, for which a \emph{spaced seed} is exploited. Essentially, a spaced seed is a mask indicating which positions of characters of the k-mer are to be ignored. Such a strategy is implemented in \clarks (see~\cite{clark:spaced}), where a set of three spaced seeds $31$-positions long with a weight of $22$ matches is used. However, if on the one hand such an implementation provides better results, on the other hand it has a high cost in time performance. This is due to the multiple reading and encoding of the same character in the sequence, which is unnecessary for contiguous k-mers.

\missh, acronym of Mutli-Iterative Spaced Seeds Hashing, is an algorithm which is addressed for a faster \emph{spaced k-mers} hashing -- where spaced k-mer indicates a k-mer with the application of a spaced seed -- when multiple spaced seeds are used. \missh exploits previously computed hashes for the hashing of new spaced k-mers in order to make fewer calculations. This is an extension of the algorithm presented in~\cite{issh}.

It is thus clear that the use of \missh within \clarks is expected to improve the time performance of the latter. In this paper we report the empirical measurement of time performances in order to assess such an improvement.

\section{Description of the Work}

\clark is written in \cpp ad in the same language \missh was implemented. There had been an integration of the \missh implementation into \clarks, but there were some bugs to be fixed. As a first step, we fixed some of such bugs and checked the compatibility of the classical \clarks results with the ones of the modified version of it.

Afterwards, we modified \clarks so to enable spaced seeds personalisation. Indeed, only three spaced seeds are used in the original software and they are hard coded in the program: there was no option for the user to change them. This was a restricted setting for the measurement of the \missh impact on time performance. Therefore, we set up a new methods for spaced seeds personalisation, still with the requirement that they must all be of the same length and weight. This part of the work is the last presented in this paper.
%Unfortunately, this part of the work is still in progress and thus there are no results to show, but it deserved be mentioned as the software used for this relation is modified accordingly.

Finally, we evaluated time performances with different settings.

The software is available at \href{https://gitlab.com/giorgio.gallina.1/clarks-missh.git}{gitlab.com/giorgio.gallina.1/clarks-missh}, where more details on the implementation are given.




\section{Experimental Setup}
\label{sec:setup}

\begin{itemize}
	\item The experiments have been carried out on a computing platform equipped with 4x Intel(R) Xeon(R) Gold 6252N CPUs at $2.30\div3.60\unit{\giga\hertz}$ and up to \hbox{$3\unit{\tera\byte}$} of available RAM. For all the executions used for a given result (i.e., for the computation of an empirical speed-up), we used the same computing units with the same setting.
	%
	\item The mode \texttt{-m 0} in the \clarks' settings is the only one tested. This is the \texttt{full mode}, that allows \emph{\QT{To get detailed results, confidence scores and other statistics}}, as stated in \clarks documentation.\\
	The only alternative would have been mode \texttt{-m 2}, which is the \emph{\QT{Express: To get results summary with the highest speed possible}}. However, this setting is incompatible with the use of \missh since it calculates only one object's spaced k-mer out of four contiguous ones, whereas \missh needs all contiguous spaced k-mers to be computed. Hence, this setting was ignored.
	\item The default \clark's bacteria database was used as target database ($6458$ targets, $23585$ sequences).
	%
	\item All the classifications are done at species level.
	%
	\item As objects to be classified, the following datasets were used:
	\begin{itemize}
		\item \emph{SRR1804065} from the Humane Microbiome Project. It contains $21873781$ reads of length $100\unit{bp}$ in fastq format (extension \texttt{.fq}).
		%
		\item Submitted \emph{ERR2270690}: $5935571$ sequences of average length $289\unit{bp}$, fastq format (\texttt{.fastq}) -- retrievable from the \href{https://www.ebi.ac.uk/ena/browser/view/ERR2270960?show=reads}{European Nucleotide Archive}.
		%
		\item A dataset of $1652131$ long reads in fasta format (\texttt{.fna}) that we have made up by merging the nine datasets in the group of name \qt{R} analysed in~\cite{metaprob}. The sequences are averagely $739\unit{bp}$ long, with a range of length $610\div987\unit{bp}$. For these datasets \missh has provided the highest speed-up among the tested ones in other studies. We will refer to this dataset as \emph{Rlong}.
		%
		\item A small dataset from the \href{https://bitbucket.org/samu661/metaprob/src/master/TestInputFile/}{MetaProb project}: $20000$ reads of length $741\unit{bp}$ in the range $646\div872\unit{bp}$ in fasta format (\texttt{.fna}). It was only used for debugging.
	\end{itemize}
\end{itemize}



%\section{Results}

\section{Debugging}
\label{sec:dbg}

In order to assess the goodness of the \missh integration, the results obtained with the original \clarks implementation and the modified one were compared. With SRR1804065 as object dataset, fewer than $0.2\%$ of the reads were differently classified. The majority of these differed in the gamma value, with a mean distance of $0.087$, and only $0.008\%$ of them differed in the first assignment.

With a second small dataset of $20$ thousand long reads to be binned, the classifications were identical. With ERR2270960 dataset, one only assignment differed at the gamma value, with a $0.0037$ discrepancy. Finally, $0.018\%$ of Rlong results differed in gamma value by an average of $0.0998$, whereas all the first assignments were equal.

The main difference between the dataset SRR1804065 and the others is that its sequences contain many \Nb characters, in addition to \Ab, \Cb, \Gb and \Tb, for unknown bases, and the management of such character in \clarks \emph{might} differ from its management by \missh. This check was out of scope for this paper and we leave it to future work.


\section{Empirical Time Performance}

In this section, we present the results of the running time measurements carried out with the original three \clarks' spaced seeds. Only the \emph{assignment time} is considered for the evaluation of the speed-up carried by \missh. This is the exact same time showed in the original \clark's output at the end of its execution and it is, in fact, the time needed to read and classify the objects. The reason for such a choice is that this was the only piece of software that was substantially modified. This implies that, for instance, the time needed for the loading of the database of discriminative spaced k-mers is ignored. It was not ignored, however, the time needed to store results, which is the same in the two versions of the software. More details on the setup can be found at \cref{sec:setup}.

The impact on time performance was measured varying the degree of parallelism exploitation and the average length of the objects to be classified.

\missh computes each object in the same manner, and so does \clarks: the computation on a read is not affected by the computation on another one. \clarks exploits parallelism by assigning different objects to different processing units. The speed-up provided by \missh is, thus, expected to be stable despite the variation in the number of employed \textsc{CPU}s. However, since augmenting the number of processors can be seen as decreasing the number of objects under classification, the results obtained with low parallelism are statistically likely to be more reliable. %Moreover, some execution failures came along with higher parallelism, reducing the data available for analysis.
Nonetheless, measuring the actual impact of the algorithm's implementation on realistic setups is still relevant.

As far as sequences' lengths are concerned, \missh goes through a non optimal transitional phase which lasts for a fixed number of sequence's positions (smaller than the length of the seeds). Therefore, the longer the reads are, the higher the impact of the stationary optimal execution of \missh is. Consequently, we expect a higher speed-up when longer reads are binned.

\subsection{Methods}
\label{sec:method}
For each setup, ten or more executions were measured and their mean value was used for the comparison. For the mean value computation, failed executions were discharged and the outliers were ignored. We considered as outlier any measurement $x \notin [\frac{m}{2},\frac{3m}{2}]$, where $m$ is the raw $0.2$-trimmed mean value for that setting. A reason for removing outliers is that the computing platform used is shared and, even if once resources are assigned to one task they are exclusively dedicated to it, some executions might be affected, for instance, by upgrades or maintenance. Ruling out these events, we are quite confident that the running conditions were homogeneous -- i.e., coming from identically distributed random variables.

\subsection{Discussion}

As depicted in \cref{fig:def}, the modified version of \clarks\ -- i.e., that exploiting \missh\ -- outperforms the original one in terms of time needed for the sequences classification. However, high parallelism results in a loss of advantage. This is probably because it involves the simultaneous processing of different segments of the dataset, with the high speed-up carried by \missh having a lower overall impact due to the reduction of sequential hashes computations, whereas the results are all printed sequentially in a single thread; consequently, some of the advantage in the hash computation is absorbed by the result printing. This explanation is coherent with the increased stability of results obtained on datasets with longer reads.

One unexpected behaviour can be spotted in the speed-up measured when binning \srr. Since the purpose of the present analysis is to verify that \missh is generally advantageous, we did not set up experiments that could show with confidence the causes of diversities in benefit. Nevertheless, we can provide some hypothesis of explanation of such odd and unexpected behaviours. Firstly, we expect differences in datasets format to have little time impact during the classification phase, but we cannot exclude it a priori. What seems more likely to cause the observed behaviour, instead, is the presence of several \Nb characters in \srr, as pointed out in \cref{sec:dbg}. This reduces significantly the number of searches in the targets hash-table and of insertions in the results queue, but both these operations require very little computation, so they seem unlikely to be the sought causes. Rather, \missh can skip an entire hash computation by knowing in advance it will fail, whereas \clarks needs to encode all the characters preceding the first unacceptable one. Why this fact should suddenly stop having a major impact when shifting from $4$ to $8$ threads is unclear, but, in addition to the increased impact of result printing, there might be segments of the dataset significantly poorer in \Nb than others, hence hijacking a computation reach in \Nb characters toward a poorer one when such segments are processed by one single processor. On the other hand, \srr is a large dataset of short reads, which makes the ratio between hashes computed and results printed much lower than the one we have when long sequences are classified: this fact alone would make the speed-up on that dataset lower than the others, in contrast with what we observed.

In any case, the relation between the amount of time exclusively dedicated to hash computation and the one necessary for other (common) operations is relevant, as corroborated by previous studies which have shown \missh to be up to $7$ time faster in hashes computation than the naive algorithm that \clarks uses. In summary, we cannot assert the causes of some unexpected behaviour, but there is no doubt that \missh brings a significant benefit in all the experimental settings tested.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{data/speedup1.pdf}
	\caption{Speed-up of \missh with respect to original implementation of \clarks. Spaced seeds \textsc{Qclk}}
	\label{fig:def}
\end{figure}




\section{Spaced Seeds Personalisation}

In order to compare time performances with different sets of spaced seeds, several modifications to the software have been necessary. At the moment, only spaced seeds of weight $22$ and length $31$ are well integrated. It is possible to use seeds of different length and weight, but this option is still not fully supported.

In the original implementation of the software, the set of spaced seeds (referred here as \textsc{Qclk}, see appendix~\ref{sec:ss}) was hard coded so to improve time performance. Accordingly, by making it possible to choose the spaced seeds to use, the time performance of \clarks is expected to get a little worse due to the cost of generalisation. An analogous cost is not paid if \missh is used since this algorithm was intentionally developed to manage different sets of spaced seeds. Notwithstanding, such a cost might be absorbed by other operations or by the structure of the spaced seeds.

As for the correctness and reliability of the classification with different spaced seeds, we put off for future work. However, we compared different results obtained with different seeds and we found them to be somehow compatible in first assignment. We also report some very draft results about time performance obtained with spaced seeds of different weight, but these are only showing that there is room for future work.

\subsection{Different Spaced Seeds with Equal Length and Weight}
\label{sec:ss22}

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{data/speedupW22a.pdf}
	\caption{Speed-up brought by \missh to \clarks with different spaced seeds of weight $22$.}
	\label{fig:ss22}
\end{figure}

We compared the executions on sets with different number of spaced seeds of length $31$ and weight $22$:
\begin{description}
	\item[\textsc{Qclk}] Original set, see appendix~\ref{sec:ss} -- $3$ seeds;
	\item[\textsc{Q4-6}] The set called \textsc{Qw22} in appendix~\ref{sec:ss} -- $3$ seeds;
	\item[\textsc{Q2-5}] The seeds from \textsc{Q2 (T58570)} to \textsc{Q5} -- $4$ seeds;
	\item[\textsc{Q1-6}] The union of \textsc{Qclk} and \textsc{Q4-6} -- $6$ seeds.
\end{description}

For these measurements, we used $2$ processing units, two different datasets and we computed the speed-up as described in \cref{sec:method} except for collecting only $8$ measurements. Once again, the results confirm the advantage of \missh, as clear from \cref{fig:ss22}. If we do not see an increased speed-up for larger sets of spaced seeds -- while expected if we consider that \missh takes advantage of a greater availability of seeds, -- we should also notice that the inverse of each seed is used as well, hence doubling the set size. Nonetheless, it is worth knowing that the seeds used, other than the original ones, were specifically designed so to minimise overlap complexity, which is favourable to \missh.

Moreover, we can appreciate that \missh speed-up is generally greater when using personalised spaced seeds, as expected due to the cost of generalisation. This effect is noticeable in \cref{tab:ss22cmp}.

\begin{table}[h]
	\caption{\textsc{Time cost of spaced seeds generalisation}. The first row refers to \clarks with its original hashing, while the second one with \missh hashing. In the last row the assignment speed-up of \clarks with \missh exploitation is reported. All the data are obtained from $7$ executions of the programs on $4$ threads on the dataset ERR2270960 as explained in \cref{sec:method}. The first column reports the average assignment time when the original set of spaced seeds is used; these data are used as reference. The second and fourth columns show average assignment time when \textsc{Q4-6} and \textsc{Q1-6} are used, respectively. The third column reports the slow-down of the execution on \textsc{Q4-6} with respect to the reference; the last column shows the slow-down on \textsc{Q1-6} with respect to the reference.}
	\label{tab:ss22cmp}
	\centering
	\begin{tabular}{>{\scshape\bfseries}cS[round-precision=2]S[round-precision=2]S[round-precision=3]S[round-precision=2]S[round-precision=3]}
		\toprule
		& {\scshape\bfseries Qclk} & \multicolumn{2}{c}{\scshape\bfseries Q4-6} & \multicolumn{2}{c}{\scshape\bfseries Q1-6} \\
		& {time $[\si{\second}]$} & {time $[\si{\second}]$} & {slow-down} & {time $[\si{\second}]$} & {slow-down} \\
		\midrule
		\csvreader[head to column names,
		late after line=\\,
		late after last line=\\\midrule,
		filter not strcmp={\algo}{Speed-up}]{data/cmpSS22.csv}{}{%
			{\algo} & \qclk & \qqt & \qqc & \qqqt & \qqqc}
		\csvreader[head to column names,
		late after line=\\,
		late after last line=\\\bottomrule,
		filter strcmp={\algo}{Speed-up}]{data/cmpSS22.csv}{}{%
			{\algo} & \qclk & \qqt & \qqc & \qqqt & \qqqc}
	\end{tabular}
\end{table}


\subsection{Different Spaced Seeds with Variable Weight}

Lastly, we report some results obtained by varying the weight of spaced seeds. The sets compared are all composed of three seeds.

As already mentioned, these results are very approximated since the management of seeds of weight different from $22$ is still problematic. They should provide just an insight of what we could expect from such a generalisation and pose some interest in continuing this work.

The setup of these measurements is the one described in \cref{sec:ss22}. We observed a sensible reduction of database size along with the reduction of seeds' weight, partly justified by the smaller size of the spaced k-mers, partly by the expected increase of non discriminative spaced k-mers, and partly suggesting that a refinement of the hash-table dimensions may be needed. Spaced seeds of weight $26$ are not supported yet, unfortunately. In such a case (i.e., $w=26$) we could expect both an advantage from the high reuse of previously computed hashes and a drawback in the size of the hash-table.

The empirical speed-up obtained are reported in \cref{fig:ssw} and \cref{tab:wsrr,tab:wrlong}. Also in this case, they are all greater than $1$. 

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{data/speedupW.pdf}
	\caption{Speed-up brought by \missh to \clarks with different spaced seeds of variable weight.}
	\label{fig:ssw}
\end{figure}


\section{Conclusion}

As seen above, \missh proved to provide a sensible speed-up when exploited in \clarks for objects classification. For example, by binning six million reads of average length $289\unit{bp}$ with only one processor, we measured an average gain of time of half an hour (\cref{tab:err}).

However, we should notice that loading the database of target-specific spaced k-mers is highly time demanding, and thus we are likely not to see a great improvement when considering small datasets in relation to the number of processors exploited. Nonetheless, because of that, it makes sense to amortise database-loading cost by classifying tons of sequences in one run; in this case, the overall speed-up should be comparable with the assignment speed-up presented in this paper.

As further development, a different set of spaced seeds might improve time-performance and, more importantly, the use of \missh algorithm for the creation of the target-specific databases may be advantageous.


\printbibliography
\nocite{*}



\section{Appendices}

\subsection{Spaced Seeds}
\label{sec:ss}

The spaces seeds used are reported in the tables below and can be found in~\cite{issh}.

\begin{center}
\begin{tabular}{rc}
	\toprule
	 \multicolumn{2}{c}{\scshape\bfseries Qclk: clark-s' spaced seeds, $k=31$, $w=22$} \\
	\midrule
	\textbf{T295 (Q1)}   & $1111011101110010111001011011111$ \\
	\textbf{T58570 (Q2)} & $1111101001110101101100111011111$ \\
	\textbf{T38570 (Q3)} & $1111101011100101101110011011111$ \\
	\bottomrule
\end{tabular}
\end{center}

\begin{center}
	\begin{tabular}{rc}
		\toprule
		\multicolumn{2}{c}{\scshape\bfseries Qw22: more spaced seeds with $k=31$, $w=22$} \\
		\midrule
		\textbf{Q4} & $1111010111010011001110111110111$ \\
		\textbf{Q5} & $1110111011101111010010110011111$ \\
		\textbf{Q6} & $1111101001011100111110101101111$ \\
%		\midrule
%		\textbf{Q7} & $1111011110011010111110101011011$ \\
%		\textbf{Q8} & $1110101011101100110100111111111$ \\
%		\textbf{Q9} & $1111110101101011100111011001111$ \\
		\bottomrule
	\end{tabular}
\end{center}

\begin{center}
	\begin{tabular}{rc}
		\toprule
		\multicolumn{2}{c}{\scshape\bfseries Qw14: Spaced seeds with $k=31$, $w=14$} \\
		\midrule
%		\textbf{Q19W14} & $1110000100110100110110001010001$ \\
		\textbf{Q20w14} & $1011000001001011111101000100001$ \\
		\textbf{Q21w14} & $1010000001001101100110000110111$ \\
%		\midrule
		\textbf{Q22w14} & $1110010001000010000111011100011$ \\
%		\textbf{Q23W14} & $1010000111100111100011000000011$ \\
%		\textbf{Q24W14} & $1000100000010000010101111111011$ \\
%		\midrule
%		\textbf{Q25W14} & $1011100000100101011100010110001$ \\
%		\textbf{Q26W14} & $1000101011100101101010001100001$ \\
%		\textbf{Q27W14} & $1011010110001010101000000010111$ \\
		\bottomrule
	\end{tabular}
\end{center}

\begin{center}
	\begin{tabular}{rc}
		\toprule
		\multicolumn{2}{c}{\scshape\bfseries Qw18: Spaced seeds with $k=31$, $w=18$} \\
		\midrule
		\textbf{Q19w18} & $1011010110010101111011010110001$ \\
		\textbf{Q20w18} & $1011001001011011111101010101001$ \\
		\textbf{Q21w18} & $1010101001011101100110000111111$ \\
		%		\midrule
		%		\textbf{Q22W18} & $1110110011000010001111011110011$ \\
		%		\textbf{Q23W18} & $1010011111101111101011000000011$ \\
		%		\textbf{Q24W18} & $1100100101010000110101111111011$ \\
		%		\midrule
		%		\textbf{Q25W18} & $1011100100110101111100010111001$ \\
		%		\textbf{Q26W18} & $1110101011100111101010011100001$ \\
		%		\textbf{Q27W18} & $1011011110001011101000100011111$ \\
		\bottomrule
	\end{tabular}
\end{center}

%\begin{center}
%	\begin{tabular}{rc}
%		\toprule
%		\multicolumn{2}{c}{\scshape\bfseries Qw26: Spaced seeds with $k=31$, $w=26$} \\
%		\midrule
%		\textbf{Q19W26} & $1111101110111011111101101111111$ \\
%		\textbf{Q20W26} & $1111111011111111111111010101101$ \\
%		\textbf{Q21W26} & $1110111011011111111110011111111$ \\
%%		\midrule
%%		\textbf{Q22W26} & $1111111111100111101111111110011$ \\
%%		\textbf{Q23W26} & $1111011111111111101011011110111$ \\
%%		\textbf{Q24W26} & $1101101101111100111111111111111$ \\
%%		\midrule
%%		\textbf{Q25W26} & $1011110101111101111111111111101$ \\
%%		\textbf{Q26W26} & $1111101111110111111110111110011$ \\
%%		\textbf{Q27W26} & $1011111111101011111010111111111$ \\
%		\bottomrule
%	\end{tabular}
%\end{center}


\subsection{Time Measurements Summaries}

Here we report the measurements collected for this presentation.

In this first three tables show the average measured assignment time for each dataset, for different number of threads, in the two concurrent implementations. The second column (\textsc{original}) shows the average execution time for classification in the original implementation of \clarks $\pm$ its standard deviation. The next column (\textsc{samp.}) reports the number of measurements available from which the results reported in the second column were obtained. Similarly, the column \textsc{missh} summarises the measured execution time for classification when \missh is exploited and the next one indicates the available number of measurements. The last column shows the empirical speed-up brought by \missh, calculated as the ratio between the two average times of the same row.

The last two tables have the same structure as the former, but they are organized per set of spaced seeds in place of the number of threads, which was fixed to two for those measurements.

\begin{table}[h]
	\caption{Summary of time performances on dataset SRR1804065.}
	\label{tab:srr}
	\centering
	\begin{tabular}{ccS[round-precision=0]cS[round-precision=0]c}
		\toprule
		{\scshape\bfseries cpus} & {\scshape\bfseries original }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries missh }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries speed-up} \\
		\midrule
		\csvreader[head to column names,
		late after line={\\},
		late after last line={\\\bottomrule}]{data/def_srr.csv}{}{%
			{\cpu} & $\sisetup{round-precision=1}\num{\clarksTime} \pm \sisetup{round-precision=1}\num{\clarksSD}$ & \clarksOccurs & $\sisetup{round-precision=1}\num{\misshTime} \pm \sisetup{round-precision=1}\num{\misshSD}$ & \misshOccurs & $\sisetup{round-precision=4}\num{\speedup}$}
	\end{tabular}
\end{table}

\begin{table}[h]
	\caption{Summary of time performances on dataset ERR2270960. }
	\label{tab:err}
	\centering
	\begin{tabular}{ccS[round-precision=0]cS[round-precision=0]c}
		\toprule
		{\scshape\bfseries cpus} & {\scshape\bfseries original }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries missh }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries speed-up} \\
		\midrule
		\csvreader[head to column names,
			late after line={\\},
			late after last line={\\\bottomrule}]{data/def_ERR227.csv}{}{%
			{\cpu} & $\sisetup{round-precision=1}\num{\clarksTime} \pm \sisetup{round-precision=1}\num{\clarksSD}$ & \clarksOccurs & $\sisetup{round-precision=1}\num{\misshTime} \pm \sisetup{round-precision=1}\num{\misshSD}$ & \misshOccurs & $\sisetup{round-precision=4}\num{\speedup}$}
	\end{tabular}
\end{table}

\begin{table}[h]
	\caption{Summary of time performances on dataset Rlong.}
	\label{tab:rlong}
	\centering
	\begin{tabular}{ccS[round-precision=0]cS[round-precision=0]c}
		\toprule
		{\scshape\bfseries cpus} & {\scshape\bfseries original }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries missh }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries speed-up} \\
		\midrule
		\csvreader[head to column names,
		late after line={\\},
		late after last line={\\\bottomrule}]{data/def_Rlong.csv}{}{%
			{\cpu} & $\sisetup{round-precision=1}\num{\clarksTime} \pm \sisetup{round-precision=1}\num{\clarksSD}$ & \clarksOccurs & $\sisetup{round-precision=1}\num{\misshTime} \pm \sisetup{round-precision=1}\num{\misshSD}$ & \misshOccurs & $\sisetup{round-precision=4}\num{\speedup}$}
	\end{tabular}
\end{table}

%\begin{table}[h]
%	\caption{Summary of time performances on dataset Rlong. Same structure as in table~\ref*{tab:err}.}
%	\label{tab:glob_rlong}
%	\centering
%	\begin{tabular}{cccc}
%		\toprule
%		{\scshape\bfseries cpus} & {\scshape\bfseries original }$[\si{\second}]$ & {\scshape\bfseries missh }$[\si{\second}]$ & {\scshape\bfseries speed-up} \\
%		\midrule
%		\csvreader[head to column names,
%		late after line={\\},
%		late after last line={\\\bottomrule}]{speedup_Rlong.csv}{}{%
%			{\cpu} & $\sisetup{round-precision=2}\num{\clarksTime} \pm \sisetup{round-precision=2}\num{\clarksSD}$ & $\sisetup{round-precision=2}\num{\misshTime} \pm \sisetup{round-precision=2}\num{\misshSD}$ & $\sisetup{round-precision=4}\num{\speedup}$}
%	\end{tabular}
%\end{table}


\begin{table}[h]
	\caption{Summary of time performances on dataset \srr with variable set of spaced seeds.}
	\label{tab:wsrr}
	\centering
	\begin{tabular}{>{\scshape\bfseries}ccS[round-precision=0]cS[round-precision=0]c}
		\toprule
		{\scshape\bfseries Seeds} & {\scshape\bfseries original }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries missh }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries speed-up} \\
		\midrule
		\csvreader[head to column names,
		late after line={\\},
		late after last line={\\\bottomrule}]{data/wtsup_srr.csv}{}{%
			{\seeds} & $\sisetup{round-precision=1}\num{\clarksTime} \pm \sisetup{round-precision=1}\num{\clarksSD}$ & \clarksOccurs & $\sisetup{round-precision=1}\num{\misshTime} \pm \sisetup{round-precision=1}\num{\misshSD}$ & \misshOccurs & $\sisetup{round-precision=4}\num{\speedup}$}
	\end{tabular}
\end{table}

\begin{table}[h]
	\caption{Summary of time performances on dataset Rlong with variable set of spaced seeds.}
	\label{tab:wrlong}
	\centering
	\begin{tabular}{>{\scshape\bfseries}ccS[round-precision=0]cS[round-precision=0]c}
		\toprule
		{\scshape\bfseries Seeds} & {\scshape\bfseries original }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries missh }$[\si{\second}]$ & {\scshape\bfseries samp.} & {\scshape\bfseries speed-up} \\
		\midrule
		\csvreader[head to column names,
		late after line={\\},
		late after last line={\\\bottomrule}]{data/wtsup_Rlong.csv}{}{%
			{\seeds} & $\sisetup{round-precision=1}\num{\clarksTime} \pm \sisetup{round-precision=1}\num{\clarksSD}$ & \clarksOccurs & $\sisetup{round-precision=1}\num{\misshTime} \pm \sisetup{round-precision=1}\num{\misshSD}$ & \misshOccurs & $\sisetup{round-precision=4}\num{\speedup}$}
	\end{tabular}
\end{table}



\end{document}
