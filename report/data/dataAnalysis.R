SpeedupAnalysis<-function(infilename, outfilename)
{
    data <- read.table(infilename, header=TRUE, sep=";", comment.char="", dec=",", quote="");
    
    setting <- sort(unique(data[,1]))
    
    raw_means <- matrix(nrow=length(setting), ncol=4, dimnames = list(setting, c("clarksTime", "misshTime", "clarksTott", "misshTott")))

    for (cpu in setting)
    {
        ccpu = as.character(cpu)
        rows <- (data$n_cpus == cpu) & (data$impl_code == 0)
        raw_means[ccpu, "clarksTime"] <- mean(data[rows, "assign_time"], trim=0.1)
        raw_means[ccpu, "clarksTott"] <- mean(data[rows, "tot_time"], trim=0.1)
        
        rows <- (data$n_cpus == cpu) & (data$impl_code == 2)
        raw_means[ccpu, "misshTime"] <- mean(data[rows, "assign_time"], trim=0.1)
        raw_means[ccpu, "misshTott"] <- mean(data[rows, "tot_time"], trim=0.1)
    }
    
    res <- matrix(nrow=length(setting), ncol=11, dimnames = list(setting, c("cpu", "clarksTime", "clarksSD", "clarksOccurs", "misshTime", "misshSD", "misshOccurs", "speedup", "clarksTott", "misshTott", "totspu")))
    res[,"cpu"] <- setting
    
    for (cpu in setting)
    {
        ccpu = as.character(cpu)
        rows <- (data$n_cpus == cpu) & (data$impl_code == 0) & (data$assign_time > (raw_means[ccpu,"clarksTime"]/2)) & (data$assign_time < (raw_means[ccpu,"clarksTime"]*3/2))
        tmp1 <- data[rows, "assign_time"]
        sd <- sd(tmp1)
        mean <- mean(tmp1)
        #weig <- (tmp1 - mean)**(-2)
        #wmean <- weighted.mean(tmp1, weig)
        #print(c(mean, wmean, sd))
        #print(weig);
        res[ccpu, "clarksTime"] <- mean#mean(tmp1)
        res[ccpu, "clarksSD"] <- sd
        res[ccpu, "clarksOccurs"] <- sum(rows)
        
        rows <- (data$n_cpus == cpu) & (data$impl_code == 2) & (data$assign_time > (raw_means[ccpu,"misshTime"]/2)) & (data$assign_time < (raw_means[ccpu,"misshTime"]*3/2))
        tmp2 <- data[rows, "assign_time"]
        sd <- sd(tmp2)
        mean <- mean(tmp2)
        #weig <- (tmp2 - mean)**(-2)
        #wmean <- weighted.mean(tmp2, weig)
        res[ccpu, "misshTime"] <- mean#mean(tmp2)
        res[ccpu, "misshSD"] <- sd
        res[ccpu, "misshOccurs"] <- sum(rows)
        
        res[ccpu, "speedup"] <- res[ccpu, "clarksTime"] /res[ccpu, "misshTime"]
        
        # htest <- t.test(x = tmp1, y = tmp2, alternative="greater")
        # print(htest)
        
        
        
        rows <- (data$n_cpus == cpu) & (data$impl_code == 0) & (data$tot_time > (raw_means[ccpu,"clarksTott"]/2)) & (data$tot_time < (raw_means[ccpu,"clarksTott"]*3/2))
        res[ccpu, "clarksTott"] <- mean(data[rows, "tot_time"])
        
        rows <- (data$n_cpus == cpu) & (data$impl_code == 2) & (data$tot_time > (raw_means[ccpu,"misshTott"]/2)) & (data$tot_time < (raw_means[ccpu,"misshTott"]*3/2))
        res[ccpu, "misshTott"] <- mean(data[rows, "tot_time"])
        
        res[ccpu, "totspu"] <- res[ccpu, "clarksTott"] /res[ccpu, "misshTott"]
    }
    
    # write.table(x=res, outfilename, quote=F, sep=";", dec=",")
    # write.table(x=res, quote=F, sep=";", dec=",")
    write.csv(x=res, file=outfilename, quote=F, row.names=F)
    write.csv(x=res, quote=F, row.names=F)
    
    return (list(raw=data, rawmean=as.data.frame(raw_means), res=as.data.frame(res)))
}





SpeedupAnalysis_noss<-function(infilename, outfilename)
{
    data <- read.table(infilename, header=TRUE, sep=";", comment.char="", dec=",", quote="");
    data$assign_time <- as.numeric(data$assign_time)
    data$tot_time <- as.numeric(data$tot_time)
    
    setting <- sort(unique(data[,1]))
    
    raw_means <- as.data.frame(matrix(nrow=length(setting), ncol=4, dimnames = list(setting, c("clarksTime", "misshTime", "clarksTott", "misshTott"))))
    
    for (cpu in setting)
    {
        ccpu = as.character(cpu)
        rows <- (data$n_cpus == cpu) & (data$impl_code == 0) & (data$seeds == "default")
        raw_means[ccpu, "clarksTime"] <- mean(data[rows, "assign_time"], trim=0.1)
        raw_means[ccpu, "clarksTott"] <- mean(data[rows, "tot_time"], trim=0.1)
        
        rows <- (data$n_cpus == cpu) & (data$impl_code == 2)  & (data$seeds == "default")
        raw_means[ccpu, "misshTime"] <- mean(data[rows, "assign_time"], trim=0.1)
        raw_means[ccpu, "misshTott"] <- mean(data[rows, "tot_time"], trim=0.1)
    }
    
    res <- matrix(nrow=length(setting), ncol=11, dimnames = list(setting, c("cpu", "clarksTime", "clarksSD", "clarksOccurs", "misshTime", "misshSD", "misshOccurs", "speedup", "clarksTott", "misshTott", "totspu")))
    res[,"cpu"] <- setting
    
    for (cpu in setting)
    {
        ccpu = as.character(cpu)
        rows <- (data$n_cpus == cpu) & (data$impl_code == 0)  & (data$seeds == "default") & (data$assign_time > (raw_means[ccpu,"clarksTime"]/2)) & (data$assign_time < (raw_means[ccpu,"clarksTime"]*3/2))
        tmp1 <- data[rows, "assign_time"]
        sd <- sd(tmp1)
        mean <- mean(tmp1)
        #weig <- (tmp1 - mean)**(-2)
        #wmean <- weighted.mean(tmp1, weig)
        #print(c(mean, wmean, sd))
        #print(weig);
        res[ccpu, "clarksTime"] <- mean#mean(tmp1)
        res[ccpu, "clarksSD"] <- sd
        res[ccpu, "clarksOccurs"] <- sum(rows)
        
        rows <- (data$n_cpus == cpu) & (data$impl_code == 2) & (data$seeds == "default") &  (data$assign_time > (raw_means[ccpu,"misshTime"]/2)) & (data$assign_time < (raw_means[ccpu,"misshTime"]*3/2))
        tmp2 <- data[rows, "assign_time"]
        sd <- sd(tmp2)
        mean <- mean(tmp2)
        #weig <- (tmp2 - mean)**(-2)
        #wmean <- weighted.mean(tmp2, weig)
        res[ccpu, "misshTime"] <- mean#mean(tmp2)
        res[ccpu, "misshSD"] <- sd
        res[ccpu, "misshOccurs"] <- sum(rows)
        
        res[ccpu, "speedup"] <- res[ccpu, "clarksTime"] /res[ccpu, "misshTime"]
        
        # htest <- t.test(x = tmp1, y = tmp2, alternative="greater")
        # print(htest)
        
        
        
        rows <- (data$n_cpus == cpu) & (data$impl_code == 0) & (data$seeds == "default") & (data$tot_time > (raw_means[ccpu,"clarksTott"]/2)) & (data$tot_time < (raw_means[ccpu,"clarksTott"]*3/2))
        res[ccpu, "clarksTott"] <- mean(data[rows, "tot_time"])
        
        rows <- (data$n_cpus == cpu) & (data$impl_code == 2) & (data$seeds == "default") & (data$tot_time > (raw_means[ccpu,"misshTott"]/2)) & (data$tot_time < (raw_means[ccpu,"misshTott"]*3/2))
        res[ccpu, "misshTott"] <- mean(data[rows, "tot_time"])
        
        res[ccpu, "totspu"] <- res[ccpu, "clarksTott"] /res[ccpu, "misshTott"]
    }
    
    # write.table(x=res, outfilename, quote=F, sep=";", dec=",")
    # write.table(x=res, quote=F, sep=";", dec=",")
    write.csv(x=res, file=outfilename, quote=F, row.names=F)
    write.csv(x=res, quote=F, row.names=F)
    
    return (list(raw=data, rawmean=as.data.frame(raw_means), res=as.data.frame(res)))
}





SpeedupAnalysis_ssW<-function(infilenames, outfilenames)
{
    legend <- data.frame(name=c("SRR1804065", "Rlong"), row.names=infilenames)
    seeds_symb <- c("Qw14", "Qw18", "Qw22")
    seeds_tab <- data.frame(code=c("spk31w14_n2-4", "spk31w18_n1-3", "spk31w22_n4-6"), row.names=seeds_symb)
    ncol=length(seeds_symb)
    speedup <- matrix(nrow=2, ncol=ncol, dimnames=list(legend$name, seeds_symb))
    counts <- matrix(nrow=2, ncol=ncol, dimnames=list(legend$name, seeds_symb))
    
    
    for (j in 1:2)
    {
        infilename <- infilenames[j]
        outfilename <- outfilenames[j]
        
        data <- read.table(infilename, header=TRUE, sep=";", comment.char="", dec=".", quote="");
        data <- data[(data$n_cpus == 2) & (data$seeds != "default"),]
        setting <- sort(unique(data$seeds))
        
        # seeds_symb <- c("CLARKS", "Qw14", "Qw18", "Qw22", "Qw26")
        # seeds_tab <- data.frame(code=c("spk31w22_n1-3", "spk31w14_n1-6", "spk31w18_n1-3", "spk31w22_n4-6", "spk31w26_n1-3"), row.names=seeds_symb)
        
        raw_means <- matrix(nrow=length(seeds_symb), ncol=4, dimnames = list(seeds_symb, c("clarksTime", "misshTime", "clarksTott", "misshTott")))
        
        for (seed in seeds_symb)
        {
            scode <- seeds_tab[seed,"code"]
            rows <- (data$seeds == scode) & (data$impl_code == 0)
            raw_means[seed, "clarksTime"] <- mean(data[rows, "assign_time"], trim=0.2)
            raw_means[seed, "clarksTott"] <- mean(data[rows, "tot_time"], trim=0.2)
            
            rows <- (data$seeds == scode) & (data$impl_code == 2)
            raw_means[seed, "misshTime"] <- mean(data[rows, "assign_time"], trim=0.2)
            raw_means[seed, "misshTott"] <- mean(data[rows, "tot_time"], trim=0.2)
        }
        
        res <- as.data.frame(matrix(nrow=length(seeds_symb), ncol=11, dimnames = list(seeds_symb, c("seeds", "clarksTime", "clarksSD", "clarksOccurs", "misshTime", "misshSD", "misshOccurs", "speedup", "clarksTott", "misshTott", "totspu"))))
        res[,"seeds"] <- seeds_symb
        
        for (seed in seeds_symb)
        {
            scode <- seeds_tab[seed,"code"]
            rows <- (data$seeds == scode) & (data$impl_code == 0) & (data$assign_time > (raw_means[seed,"clarksTime"]/2)) & (data$assign_time < (raw_means[seed,"clarksTime"]*3/2))
            tmp1 <- data[rows, "assign_time"]
            sd <- sd(tmp1)
            mean <- mean(tmp1)
            #weig <- (tmp1 - mean)**(-2)
            #wmean <- weighted.mean(tmp1, weig)
            #print(c(mean, wmean, sd))
            #print(weig);
            res[seed, "clarksTime"] <- mean#mean(tmp1)
            res[seed, "clarksSD"] <- sd
            res[seed, "clarksOccurs"] <- sum(rows)
            
            rows <- (data$seeds == scode) & (data$impl_code == 2) & (data$assign_time > (raw_means[seed,"misshTime"]/2)) & (data$assign_time < (raw_means[seed,"misshTime"]*3/2))
            tmp2 <- data[rows, "assign_time"]
            sd <- sd(tmp2)
            mean <- mean(tmp2)
            #weig <- (tmp2 - mean)**(-2)
            #wmean <- weighted.mean(tmp2, weig)
            res[seed, "misshTime"] <- mean#mean(tmp2)
            res[seed, "misshSD"] <- sd
            res[seed, "misshOccurs"] <- sum(rows)
            
            res[seed, "speedup"] <- res[seed, "clarksTime"] / res[seed, "misshTime"]
            
            speedup[legend[infilename,], seed] <- res[seed, "speedup"]
            counts[legend[infilename,], seed] <- paste(res[seed, "clarksOccurs"], res[seed, "misshOccurs"], sep="/")
            
            
            rows <- (data$seeds == scode) & (data$impl_code == 0) & (data$tot_time > (raw_means[seed,"clarksTott"]/2)) & (data$tot_time < (raw_means[seed,"clarksTott"]*3/2))
            res[seed, "clarksTott"] <- mean(data[rows, "tot_time"])
            
            rows <- (data$seeds == scode) & (data$impl_code == 2) & (data$tot_time > (raw_means[seed,"misshTott"]/2)) & (data$tot_time < (raw_means[seed,"misshTott"]*3/2))
            res[seed, "misshTott"] <- mean(data[rows, "tot_time"])
            
            res[seed, "totspu"] <- res[seed, "clarksTott"] /res[seed, "misshTott"]
        }
        
        # write.table(x=res, outfilename, quote=F, sep=";", dec=",")
        # write.table(x=res, quote=F, sep=";", dec=",")
        write.csv(x=res, file=outfilename, quote=F, row.names=F)
        write.csv(x=res, quote=F, row.names=F)
    }
    
    
    pdf("speedupW.pdf", width=16*5/9, height=5);
    
    bp <- barplot(height=speedup, beside=T, legend.text=T, xlab="Spaced Seeds Set", ylab="speedup", main="Speedup with Variably-Weighted Seeds", ylim=c(0,5), axis.lty=1)
    # legend("topleft", legend=legend$name, fill="grey")
    abline(h=1, lty=3)
    
    # text(bp, speedup+0.05, counts, cex=0.9)
    text(bp, speedup+0.2, round(speedup, 3), cex=1.2)
    
    dev.off()
    
    # return (list(raw=data, rawmean=as.data.frame(raw_means), res=as.data.frame(res)))
}


SpeedupAnalysis_ss22cmp<-function(infilename, outfilename)
{
    legend <- data.frame(name=c("CLARK-S", "MISSH"), row.names=c(0,2))
    seeds_symb <- c("QCLK", "Q4-6", "Q1-6")
    seeds_tab <- data.frame(code=c("spk31w22_n1-3", "spk31w22_n4-6", "spk31w22_n1-6"), row.names=seeds_symb)
    
    
    data <- read.table(infilename, header=TRUE, sep=";", comment.char="", dec=".", quote="");
    data <- data[(data$n_cpus == 4) & (data$seeds != "default"),]
    setting <- sort(unique(data$seeds))
    
    raw_means <- matrix(nrow=length(seeds_symb), ncol=4, dimnames = list(seeds_symb, c("clarksTime", "misshTime", "clarksTott", "misshTott")))
    
    for (seed in seeds_symb)
    {
        scode <- seeds_tab[seed,"code"]
        rows <- (data$seeds == scode) & (data$impl_code == 0)
        raw_means[seed, "clarksTime"] <- mean(data[rows, "assign_time"], trim=0.2)
        raw_means[seed, "clarksTott"] <- mean(data[rows, "tot_time"], trim=0.2)
        
        rows <- (data$seeds == scode) & (data$impl_code == 2)
        raw_means[seed, "misshTime"] <- mean(data[rows, "assign_time"], trim=0.2)
        raw_means[seed, "misshTott"] <- mean(data[rows, "tot_time"], trim=0.2)
    }
    
    res <- as.data.frame(matrix(nrow=length(seeds_symb), ncol=11, dimnames = list(seeds_symb, c("seeds", "clarksTime", "clarksSD", "clarksOccurs", "misshTime", "misshSD", "misshOccurs", "speedup", "clarksTott", "misshTott", "totspu"))))
    res[,"seeds"] <- seeds_symb
    
    for (seed in seeds_symb)
    {
        scode <- seeds_tab[seed,"code"]
        rows <- (data$seeds == scode) & (data$impl_code == 0) & (data$assign_time > (raw_means[seed,"clarksTime"]/2)) & (data$assign_time < (raw_means[seed,"clarksTime"]*3/2))
        tmp1 <- data[rows, "assign_time"]
        sd <- sd(tmp1)
        mean <- mean(tmp1)
        res[seed, "clarksTime"] <- mean
        res[seed, "clarksSD"] <- sd
        res[seed, "clarksOccurs"] <- sum(rows)
        
        rows <- (data$seeds == scode) & (data$impl_code == 2) & (data$assign_time > (raw_means[seed,"misshTime"]/2)) & (data$assign_time < (raw_means[seed,"misshTime"]*3/2))
        tmp2 <- data[rows, "assign_time"]
        sd <- sd(tmp2)
        mean <- mean(tmp2)
        res[seed, "misshTime"] <- mean
        res[seed, "misshSD"] <- sd
        res[seed, "misshOccurs"] <- sum(rows)
        
        res[seed, "speedup"] <- res[seed, "clarksTime"] / res[seed, "misshTime"]
        
        
        rows <- (data$seeds == scode) & (data$impl_code == 0) & (data$tot_time > (raw_means[seed,"clarksTott"]/2)) & (data$tot_time < (raw_means[seed,"clarksTott"]*3/2))
        res[seed, "clarksTott"] <- mean(data[rows, "tot_time"])
        
        rows <- (data$seeds == scode) & (data$impl_code == 2) & (data$tot_time > (raw_means[seed,"misshTott"]/2)) & (data$tot_time < (raw_means[seed,"misshTott"]*3/2))
        res[seed, "misshTott"] <- mean(data[rows, "tot_time"])
        
        res[seed, "totspu"] <- res[seed, "clarksTott"] /res[seed, "misshTott"]
    }
    
    # write.table(x=res, outfilename, quote=F, sep=";", dec=",")
    # write.table(x=res, quote=F, sep=";", dec=",")
    # write.csv(x=res, file=outfilename, quote=F, row.names=F)
    # write.csv(x=res, quote=F, row.names=F)
    
    indx <- c("clarksTime", "misshTime", "speedup")
    algo <- c("CLARK-S", "MISSH", "Speed-up")
    qclk=as.vector(t(res["QCLK",indx]))
    qqt=as.vector(t(res["Q4-6", indx]))
    qqc=as.vector(t(res["Q4-6",indx] / res["QCLK",indx]))
    qqqt=as.vector(t(res["Q1-6", indx]))
    qqqc=as.vector(t(res["Q1-6",indx] / res["QCLK",indx]))
    comparison <- data.frame(algo=algo, qclk=qclk, qqt=qqt, qqc=qqc, qqqt=qqqt, qqqc=qqqc)
    write.csv(x=comparison, file=outfilename, quote=F, row.names=F)
    write.csv(x=comparison, quote=F, row.names=F)
    
    return (comparison)

    #return (list(raw=data, rawmean=as.data.frame(raw_means), res=as.data.frame(res)))
}



SpeedupAnalysis_ss22a<-function(infilenames, outfilenames)
{
    legend <- data.frame(name=c("SRR1804065", "Rlong"), row.names=infilenames)
    seeds_symb <- c("QCLK", "Q4-6", "Q2-5", "Q1-6")
    seeds_tab <- data.frame(code=c("spk31w22_n1-3", "spk31w22_n4-6", "spk31w22_n2-5", "spk31w22_n1-6"), row.names=seeds_symb)
    ncol=length(seeds_symb)
    speedup <- matrix(nrow=2, ncol=ncol, dimnames=list(legend$name, seeds_symb))
    counts <- matrix(nrow=2, ncol=ncol, dimnames=list(legend$name, seeds_symb))
    
    
    for (j in 1:2)
    {
        infilename <- infilenames[j]
        outfilename <- outfilenames[j]
        
        data <- read.table(infilename, header=TRUE, sep=";", comment.char="", dec=".", quote="");
        data <- data[(data$n_cpus == 2) & (data$seeds != "default"),]
        setting <- sort(unique(data$seeds))
        
        raw_means <- matrix(nrow=length(seeds_symb), ncol=4, dimnames = list(seeds_symb, c("clarksTime", "misshTime", "clarksTott", "misshTott")))
        
        for (seed in seeds_symb)
        {
            scode <- seeds_tab[seed,"code"]
            rows <- (data$seeds == scode) & (data$impl_code == 0)
            raw_means[seed, "clarksTime"] <- mean(data[rows, "assign_time"], trim=0.2)
            raw_means[seed, "clarksTott"] <- mean(data[rows, "tot_time"], trim=0.2)
            
            rows <- (data$seeds == scode) & (data$impl_code == 2)
            raw_means[seed, "misshTime"] <- mean(data[rows, "assign_time"], trim=0.2)
            raw_means[seed, "misshTott"] <- mean(data[rows, "tot_time"], trim=0.2)
        }
        
        res <- as.data.frame(matrix(nrow=length(seeds_symb), ncol=11, dimnames = list(seeds_symb, c("seeds", "clarksTime", "clarksSD", "clarksOccurs", "misshTime", "misshSD", "misshOccurs", "speedup", "clarksTott", "misshTott", "totspu"))))
        res[,"seeds"] <- seeds_symb
        
        for (seed in seeds_symb)
        {
            scode <- seeds_tab[seed,"code"]
            rows <- (data$seeds == scode) & (data$impl_code == 0) & (data$assign_time > (raw_means[seed,"clarksTime"]/2)) & (data$assign_time < (raw_means[seed,"clarksTime"]*3/2))
            tmp1 <- data[rows, "assign_time"]
            sd <- sd(tmp1)
            mean <- mean(tmp1)
            #weig <- (tmp1 - mean)**(-2)
            #wmean <- weighted.mean(tmp1, weig)
            #print(c(mean, wmean, sd))
            #print(weig);
            res[seed, "clarksTime"] <- mean#mean(tmp1)
            res[seed, "clarksSD"] <- sd
            res[seed, "clarksOccurs"] <- sum(rows)
            
            rows <- (data$seeds == scode) & (data$impl_code == 2) & (data$assign_time > (raw_means[seed,"misshTime"]/2)) & (data$assign_time < (raw_means[seed,"misshTime"]*3/2))
            tmp2 <- data[rows, "assign_time"]
            sd <- sd(tmp2)
            mean <- mean(tmp2)
            #weig <- (tmp2 - mean)**(-2)
            #wmean <- weighted.mean(tmp2, weig)
            res[seed, "misshTime"] <- mean#mean(tmp2)
            res[seed, "misshSD"] <- sd
            res[seed, "misshOccurs"] <- sum(rows)
            
            res[seed, "speedup"] <- res[seed, "clarksTime"] / res[seed, "misshTime"]
            
            speedup[legend[infilename,], seed] <- res[seed, "speedup"]
            counts[legend[infilename,], seed] <- paste(res[seed, "clarksOccurs"], res[seed, "misshOccurs"], sep="/")
            
            
            
            rows <- (data$seeds == scode) & (data$impl_code == 0) & (data$tot_time > (raw_means[seed,"clarksTott"]/2)) & (data$tot_time < (raw_means[seed,"clarksTott"]*3/2))
            res[seed, "clarksTott"] <- mean(data[rows, "tot_time"])
            
            rows <- (data$seeds == scode) & (data$impl_code == 2) & (data$tot_time > (raw_means[seed,"misshTott"]/2)) & (data$tot_time < (raw_means[seed,"misshTott"]*3/2))
            res[seed, "misshTott"] <- mean(data[rows, "tot_time"])
            
            res[seed, "totspu"] <- res[seed, "clarksTott"] /res[seed, "misshTott"]
        }
        
        # write.table(x=res, outfilename, quote=F, sep=";", dec=",")
        # write.table(x=res, quote=F, sep=";", dec=",")
        write.csv(x=res, file=outfilename, quote=F, row.names=F)
        write.csv(x=res, quote=F, row.names=F)
    }
    
    
    pdf("speedupW22a.pdf", width=16*5/9, height=5);
    
    bp <- barplot(height=speedup, beside=T, legend.text=T, xlab="Spaced Seeds Set", ylab="speedup", main="Speedup with Original Seeds", ylim=c(0,2.75), axis.lty=1, args.legend=list(x="topleft"))
    # legend("topleft", legend=legend$name, fill="grey")
    abline(h=1, lty=3)
    
    # text(bp, speedup+0.05, counts, cex=0.9)
    text(bp, speedup+0.1, round(speedup, 3), cex=1.2)
    
    dev.off()
    
    return (list(raw=data, rawmean=as.data.frame(raw_means), res=as.data.frame(res), barplot=bp))
}







SpeedupAnalysis_ssWtot<-function(infilenames, outfilenames)
{
    legend <- data.frame(name=c("SRR1804065", "Rlong"), row.names=infilenames)
    seeds_symb <- c("Qclk", "Qw22", "Q2-5", "Q1-6", "Qw18","Qw14")
    seeds_tab <- data.frame(code=c("spk31w22_n1-3", "spk31w22_n4-6", "spk31w22_n2-5", "spk31w22_n1-6", "spk31w18_n1-3", "spk31w14_n2-4"), row.names=seeds_symb)
    ncol=length(seeds_symb)
    speedup <- matrix(nrow=2, ncol=ncol, dimnames=list(legend$name, seeds_symb))
    counts <- matrix(nrow=2, ncol=ncol, dimnames=list(legend$name, seeds_symb))
    
    
    for (j in 1:2)
    {
        infilename <- infilenames[j]
        outfilename <- outfilenames[j]
        
        data <- read.table(infilename, header=TRUE, sep=";", comment.char="", dec=".", quote="");
        data <- data[(data$n_cpus == 2) & (data$seeds != "default"),]
        setting <- sort(unique(data$seeds))
        
        # seeds_symb <- c("CLARKS", "Qw14", "Qw18", "Qw22", "Qw26")
        # seeds_tab <- data.frame(code=c("spk31w22_n1-3", "spk31w14_n1-6", "spk31w18_n1-3", "spk31w22_n4-6", "spk31w26_n1-3"), row.names=seeds_symb)
        
        raw_means <- matrix(nrow=length(seeds_symb), ncol=4, dimnames = list(seeds_symb, c("clarksTime", "misshTime", "clarksTott", "misshTott")))
        
        for (seed in seeds_symb)
        {
            scode <- seeds_tab[seed,"code"]
            rows <- (data$seeds == scode) & (data$impl_code == 0)
            raw_means[seed, "clarksTime"] <- mean(data[rows, "assign_time"], trim=0.2)
            raw_means[seed, "clarksTott"] <- mean(data[rows, "tot_time"], trim=0.2)
            
            rows <- (data$seeds == scode) & (data$impl_code == 2)
            raw_means[seed, "misshTime"] <- mean(data[rows, "assign_time"], trim=0.2)
            raw_means[seed, "misshTott"] <- mean(data[rows, "tot_time"], trim=0.2)
        }
        
        res <- as.data.frame(matrix(nrow=length(seeds_symb), ncol=11, dimnames = list(seeds_symb, c("seeds", "clarksTime", "clarksSD", "clarksOccurs", "misshTime", "misshSD", "misshOccurs", "speedup", "clarksTott", "misshTott", "totspu"))))
        res[,"seeds"] <- seeds_symb
        
        for (seed in seeds_symb)
        {
            scode <- seeds_tab[seed,"code"]
            rows <- (data$seeds == scode) & (data$impl_code == 0) & (data$assign_time > (raw_means[seed,"clarksTime"]/2)) & (data$assign_time < (raw_means[seed,"clarksTime"]*3/2))
            tmp1 <- data[rows, "assign_time"]
            sd <- sd(tmp1)
            mean <- mean(tmp1)
            #weig <- (tmp1 - mean)**(-2)
            #wmean <- weighted.mean(tmp1, weig)
            #print(c(mean, wmean, sd))
            #print(weig);
            res[seed, "clarksTime"] <- mean#mean(tmp1)
            res[seed, "clarksSD"] <- sd
            res[seed, "clarksOccurs"] <- sum(rows)
            
            rows <- (data$seeds == scode) & (data$impl_code == 2) & (data$assign_time > (raw_means[seed,"misshTime"]/2)) & (data$assign_time < (raw_means[seed,"misshTime"]*3/2))
            tmp2 <- data[rows, "assign_time"]
            sd <- sd(tmp2)
            mean <- mean(tmp2)
            #weig <- (tmp2 - mean)**(-2)
            #wmean <- weighted.mean(tmp2, weig)
            res[seed, "misshTime"] <- mean#mean(tmp2)
            res[seed, "misshSD"] <- sd
            res[seed, "misshOccurs"] <- sum(rows)
            
            res[seed, "speedup"] <- res[seed, "clarksTime"] / res[seed, "misshTime"]
            
            speedup[legend[infilename,], seed] <- res[seed, "speedup"]
            counts[legend[infilename,], seed] <- paste(res[seed, "clarksOccurs"], res[seed, "misshOccurs"], sep="/")
            
            
            rows <- (data$seeds == scode) & (data$impl_code == 0) & (data$tot_time > (raw_means[seed,"clarksTott"]/2)) & (data$tot_time < (raw_means[seed,"clarksTott"]*3/2))
            res[seed, "clarksTott"] <- mean(data[rows, "tot_time"])
            
            rows <- (data$seeds == scode) & (data$impl_code == 2) & (data$tot_time > (raw_means[seed,"misshTott"]/2)) & (data$tot_time < (raw_means[seed,"misshTott"]*3/2))
            res[seed, "misshTott"] <- mean(data[rows, "tot_time"])
            
            res[seed, "totspu"] <- res[seed, "clarksTott"] /res[seed, "misshTott"]
        }
        
        # write.table(x=res, outfilename, quote=F, sep=";", dec=",")
        # write.table(x=res, quote=F, sep=";", dec=",")
        write.csv(x=res, file=outfilename, quote=F, row.names=F)
        write.csv(x=res, quote=F, row.names=F)
    }
    
    
    # pdf("speedupW.pdf", width=16*5/9, height=5);
    # 
    # bp <- barplot(height=speedup, beside=T, legend.text=T, xlab="Spaced Seeds Set", ylab="speedup", main="Speedup with Variably-Weighted Seeds", axis.lty=1)
    # # legend("topleft", legend=legend$name, fill="grey")
    # abline(h=1, lty=3)
    # 
    # # text(bp, speedup+0.05, counts, cex=0.9)
    # text(bp, speedup+0.3, round(speedup, 3), cex=1.2)
    # 
    # dev.off()
    
    # return (list(raw=data, rawmean=as.data.frame(raw_means), res=as.data.frame(res)))
}












SpeedupAnalysis_noss_plot<-function(infilenames)
{
    legend <- data.frame(name=c("SRR1804065", "ERR2270960", "Rlong"), row.names=infilenames)
    xcpu   <- c(1, 2, 4, 8, 16, 32, 64)
    ncol=length(xcpu)
    speedup <- matrix(nrow=3, ncol=ncol, dimnames=list(legend$name, xcpu))
    counts <- matrix(nrow=3, ncol=ncol, dimnames=list(legend$name, xcpu))
    
    for (filename in infilenames)
    {
        data <- read.table(filename, header=TRUE, sep=";", comment.char="", dec=",", quote="");
        data$assign_time <- as.numeric(data$assign_time)
        data$tot_time <- as.numeric(data$tot_time)
        
        setting <- sort(unique(data[,1]))
        
        raw_means <- as.data.frame(matrix(nrow=length(setting), ncol=4, dimnames = list(setting, c("clarksTime", "misshTime", "clarksTott", "misshTott"))))
        
        for (cpu in setting)
        {
            ccpu = as.character(cpu)
            rows <- (data$n_cpus == cpu) & (data$impl_code == 0) & (data$seeds == "default")
            raw_means[ccpu, "clarksTime"] <- mean(data[rows, "assign_time"], trim=0.1)
            raw_means[ccpu, "clarksTott"] <- mean(data[rows, "tot_time"], trim=0.1)
            
            rows <- (data$n_cpus == cpu) & (data$impl_code == 2)  & (data$seeds == "default")
            raw_means[ccpu, "misshTime"] <- mean(data[rows, "assign_time"], trim=0.1)
            raw_means[ccpu, "misshTott"] <- mean(data[rows, "tot_time"], trim=0.1)
        }
        
        res <- matrix(nrow=length(setting), ncol=11, dimnames = list(setting, c("cpu", "clarksTime", "clarksSD", "clarksOccurs", "misshTime", "misshSD", "misshOccurs", "speedup", "clarksTott", "misshTott", "totspu")))
        res[,"cpu"] <- setting
        
        for (cpu in setting)
        {
            ccpu = as.character(cpu)
            rows <- (data$n_cpus == cpu) & (data$impl_code == 0)  & (data$seeds == "default") & (data$assign_time > (raw_means[ccpu,"clarksTime"]/2)) & (data$assign_time < (raw_means[ccpu,"clarksTime"]*3/2))
            tmp1 <- data[rows, "assign_time"]
            sd <- sd(tmp1)
            mean <- mean(tmp1)
            #weig <- (tmp1 - mean)**(-2)
            #wmean <- weighted.mean(tmp1, weig)
            #print(c(mean, wmean, sd))
            #print(weig);
            res[ccpu, "clarksTime"] <- mean#mean(tmp1)
            res[ccpu, "clarksSD"] <- sd
            res[ccpu, "clarksOccurs"] <- sum(rows)
            
            rows <- (data$n_cpus == cpu) & (data$impl_code == 2) & (data$seeds == "default") &  (data$assign_time > (raw_means[ccpu,"misshTime"]/2)) & (data$assign_time < (raw_means[ccpu,"misshTime"]*3/2))
            tmp2 <- data[rows, "assign_time"]
            sd <- sd(tmp2)
            mean <- mean(tmp2)
            #weig <- (tmp2 - mean)**(-2)
            #wmean <- weighted.mean(tmp2, weig)
            res[ccpu, "misshTime"] <- mean#mean(tmp2)
            res[ccpu, "misshSD"] <- sd
            res[ccpu, "misshOccurs"] <- sum(rows)
            
            res[ccpu, "speedup"] <- res[ccpu, "clarksTime"] /res[ccpu, "misshTime"]
            speedup[legend[filename,], ccpu] <- res[ccpu, "speedup"]
            counts[legend[filename,], ccpu] <- paste(res[ccpu, "clarksOccurs"], res[ccpu, "misshOccurs"], sep="/")
            
            
            rows <- (data$n_cpus == cpu) & (data$impl_code == 0) & (data$seeds == "default") & (data$tot_time > (raw_means[ccpu,"clarksTott"]/2)) & (data$tot_time < (raw_means[ccpu,"clarksTott"]*3/2))
            res[ccpu, "clarksTott"] <- mean(data[rows, "tot_time"])
            
            rows <- (data$n_cpus == cpu) & (data$impl_code == 2) & (data$seeds == "default") & (data$tot_time > (raw_means[ccpu,"misshTott"]/2)) & (data$tot_time < (raw_means[ccpu,"misshTott"]*3/2))
            res[ccpu, "misshTott"] <- mean(data[rows, "tot_time"])
            
            res[ccpu, "totspu"] <- res[ccpu, "clarksTott"] /res[ccpu, "misshTott"]
        }
    }
    
    pdf("speedup1.pdf", width=10, height=5);
    
    bp <- barplot(height=speedup, beside=T, legend.text=T, xlab="Number of threads", ylab="speedup", main="Speedup with Original Seeds", ylim=c(0,2.4), axis.lty=1)
    abline(h=1, lty=3)
    
    # text(bp, speedup+0.05, counts, cex=0.9)
    text(bp, speedup+0.05, round(speedup, 2), cex=0.9)
    
    dev.off()
    
    return (list(raw=data, rawmean=as.data.frame(raw_means), res=as.data.frame(res), bar.plot=bp))
}