#!/bin/bash

RPT=$(readlink -f "$0")
LDIR=$(dirname "$FSCRPT")

compare_progr=$1
objf=$2
summary=$3

for cpu1 in 1 2 4 8 16 32 64 
do
    for cpu2 in 1 2 4 8 16 32 64
    do
        for file1 in $LDIR/CPU$cpu1/res0_$objf*
        do
            for file2 in $LDIR/CPU$cpu2/res2_$objf*
            do
                $LDIR/$compare_progr $file1 $file2 $summary
            done
        done
    done
done
