#include<iostream>
#include<fstream>
#include<cstring>
#include<cmath>
using namespace std;

int split_line(char **tokens, char* line)
{
	char del[] = ",";
	char* pch = strtok(line, del);
	int size = 0;
	while(pch != NULL)
	{
		tokens[size] = new char[strlen(pch)+1];
		strcpy(tokens[size++], pch);
		pch = strtok(NULL,",");
	}
	return size;
}

void analyse_correspondence(double* summary, char* line1, char* line2)
{
	char *lines1[20], *lines2[20];
	int s1, s2 = -1;
	
	s1 = split_line(lines1, line1);
	s2 = split_line(lines2, line2);
	
	// corrupted line
	if( (s1 != s2) || s1 < 0 || s1 > 8)
	{
		summary[10]++;
		return;
	}
	
	// object identifier: must be equal, otherwise other measures have no sense
	if(strcmp(lines1[0], lines2[0]) == 0)
	{
		// compare obj length, should be equal (otherwise there were some errors in reading the file
		if(strcmp(lines1[1], lines2[1]) != 0)
		{	summary[1]++; }
		// compare gamma (count different and sum square distance
		if(strcmp(lines1[2], lines2[2]) != 0)
		{
			summary[2]++;
			double g1 = stod(lines1[2]);
			double g2 = stod(lines2[2]);
			summary[3] += abs(g1-g2);
		}
		// 1st assignment
		if(strcmp(lines1[3], lines2[3]) != 0)
		{	summary[4]++; }
		// 1st score
		if(strcmp(lines1[4], lines2[4]) != 0)
		{	summary[5]++; }
		// 2nd assignment
		if(strcmp(lines1[5], lines2[5]) != 0)
		{	summary[6]++; }
		// 2nd score
		if(strcmp(lines1[6], lines2[6]) != 0)
		{	summary[7]++; }
		// confidence
		if(strcmp(lines1[7], lines2[7]) != 0)
		{
			summary[8]++;
			double c1 = stod(lines1[7]);
			double c2 = stod(lines2[7]);
			summary[9] += abs(c1-c2);
		}
	}else
	{
		summary[0]++;
	}
	
	return;
}

int main(int argn, char** argv)
{
	/*
	 char str[] = "Hello,world,12.4,345,ehd,";
	 char **tokens = new char*[12];
	
	 int size = split_line(tokens, str);
	 cout << size << endl;
	 
	 for(int i = 0; i < size; i++)
	 {
		 cout << tokens[i] << " - ";
	 }
	
	 return 0;*/



	// if(argn < 5)
	// {
		// cerr << "Usage: " << argv[0] << " <filename1> <filename2> <extended_output_filename> <summary_output_filename>" << endl;
		// return 1;
	// }
	
	if(argn < 4)
	{
		cerr << "Usage: " << argv[0] << " <filename1> <filename2> <summary_output_filename>" << endl;
		return 1;
	}

	string fname1 = argv[1];
	string fname2 = argv[2];
	// string outfn  = argv[3];
	string sumr_fn  = argv[3];

	ifstream f1(fname1);
	ifstream f2(fname2);
	// ofstream of(outfn);
	ofstream write_summary(sumr_fn, fstream::out | fstream::app);
	
	// summary variables
	// obj, obj_len, n_gamma, gamma_dist, 1assign, 1score, 2assign, 2score, n_confd, confd_dist, corrupted, lines
	double summary[12] = {0,0,0,0,0,0,0,0,0,0,0,0};

	/*if(!of.is_open())
	{   cerr << "Could not open output file" << outfn << endl;
	}else*/
	if(!write_summary.is_open())
	{	cerr << "Output file" << sumr_fn << " could not be opened." << endl;
	}else if(f1.is_open())
	{
		if(f2.is_open())
		{
			// output header if not present
			if(write_summary.tellp() == 0)
			{
				write_summary << "n_objID;n_objLEN;n_gamma;gamma_avgDist;n_1assign;n_1score;n_2assign;n_2score;n_confidence;confidence_avgDist;n_corrupted;n_lines" << endl;
			}
			
			char line1[1024], line2[1024];
			long count_diff = 0, count_line = 0;
			while(!f1.eof())
			{
				if(f1.getline(line1, 1024))
				{
					count_line++;
					if(f2.getline(line2, 1024))
					{       
						if(strcmp(line1, line2) != 0)
						{
							count_diff++;
							
							// extended comparison
							// of << count_line << endl;
							// of << line1 << endl << line2 << endl << endl;
							
							// summary
							analyse_correspondence(summary, line1, line2);
							
						}
					}else
					{       
						// of << count_line << endl;
						// of << "Missing line from " << fname2 << endl << endl;
						summary[10]++; //corrupted
						count_diff++;
					}
				}
			}

			while(!f2.eof())
			{
				if(f2.getline(line2, 1024))
				{   count_line++;
					count_diff++;
					// of << count_line << endl;
					// of << "Missing line from " << fname1 << endl << endl;
					summary[10]++;
				}
			}

			cout << "The two files have " << count_diff << " different lines out of " << count_line << endl;
			// of << "The two files have " << count_diff << " different lines out of " << count_line << endl;
			
			// summary[10] = count_diff;
			// mean distance between gammas
			summary[3] = summary[3] / summary[2];
			// mean distance between confidences
			summary[9] = summary[9] / summary[8];
			// print results
			for(int i = 0; i < 11; i++)
			{
				write_summary << summary[i] << ";";
			}
			write_summary << count_diff << endl;
			
		}else
		{
			cerr << "Output file" << fname2 << " could not be opened." << endl;
			f2.close();
			f1.close();
			write_summary.close();
			// of.close();
			return 1;
		}
	}else
	{
		cerr << "Output file" << fname1 << " could not be opened." << endl;
		f1.close();
		f2.close();
		write_summary.close();
		// of.close();
		return 1;
	}

	f1.close();
	f2.close();
	write_summary.close();
	// of.close();
	return 0;
}
