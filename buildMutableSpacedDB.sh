#!/bin/sh
# 
#   CLARK, CLAssifier based on Reduced K-mers.
# 
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Copyright 2013-2019, Rachid Ounit <rouni001@cs.ucr.edu>
#   buildSpacedDB.sh: To build databases of discriminative spaced k-mers 
#			from the the database of discriminative 31-mers.
#


RPT=$(readlink -f "$0")
LDIR=$(dirname "$FSCRPT")

if [ ! -s "$LDIR/.dbAddress" ]; then
echo "Please run the script set_targets.sh to define the targets."
exit
fi

DIR=""
for db in `cat $LDIR/.dbAddress`
do
DIR="$db"
done

if [ ! -s $DIR/db_central_k$1_t*_s1610612741_m0.tsk.sz ]; then
echo "Failed to find the database of discriminative $1-mers."
exit
fi

echo "Spaced seeds of length $1 and weight $2 retrieved from file \"$3\."
$LDIR/exe/.spacedBashWriter $1 $2 $3 

chmod u+x $LDIR/.buildModifiedSpDB_k$1w$2.bash

echo "Spaced seeds loaded."
echo ""
echo "BUILDING DATABASES."

$LDIR/.buildModifiedSpDB_k$1w$2.bash



# ####################################################################################
