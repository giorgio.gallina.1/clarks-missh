# CLARK-S + MISSH

CLARK-S from http://clark.cs.ucr.edu/ modified with the integration of ISSH for multiple spaced seeds (MISSH) from https://github.com/CominLab/ISSH for faster assignment.
- CLARK paper: https://doi.org/10.1186/s12864-015-1419-2
- ISSH paper:  https://doi.org/10.1089/cmb.2019.0298

Please refer to CLARK documentation for all the details about such software. Here we only describe the modifications from the original project.



## Description

CLARK, CLAssifier based on Reduced K-mers, is a DNA sequences classifier which, in its CLARK-S version, uses spaced seeds for the (meta)genomics binning.

MISSH, Multi-Iterative Spaced Seeds Hashing, is an algorithm for fast spaced k-mers hashing.

The aim of this project is to speedup CLARK-S' classification by exploiting MISSH algorithm. Since MISSH targets the exact same computational problem that is targeted by CLARK-S too, there should be no differences on the results, but only on time performance.

However, the original CLARK-S algorithm is kept as default execution strategy. Use `-i 2` (implementation code = 2) as option in order to run CLARK-S exploiting MISSH.

With the purpose of measuring time performances with different settings, **personalization of spaced seeds** is also explored in this project.


## Installation

No changes from the original version of CLARK. Just execute the installation script ("./install.sh") from the terminal console. C++17 or later is required.


## Execution

Few running options are added.
- `-i <implementation_code>`, where `implementation_code` is either `0` for original CLARK-S execution or `2` for the modified version. This setting was designed to measure performance differences in a coherent way, or to choose the more convenient implementation to run for the case at hand. It only has effect when `-m 0` is setted as well.
- `-S <spaced_seeds_filename>` for spaced seeds modification as described below.
- `-w <spaced_seeds_weight>` to declare the weight of the personalized spaced seeds. This value *must* be set somewhere after `-k` in order for the check *w in [2..k]* to be meaningful. Also, it has no sense to state `-w` when the option `-S` is not used.

An example of execution command is the following:
```
$ ./CLARK-S -T ./targets_addresses.txt -D ./DBD/ -O ./objects.fa -R ./results -m 0 -i 2
```


## Spaced Seeds Modification

Since CLARK-S only uses three hard coded spaced seeds, many changes had to be made in order to let CLARK-S use a personalized set of spaced seeds. Some steps are necessary to exploit such an extension.
**However, this part of the project still needs to be tested to guarantee exactness of the results.**

### Spaced seeds definition
The set of spaced seeds you are willing to use should be codified in a simple txt file. Keep in mind that, even with this extension, *all the seeds in the personalized set must have the same length and the same weight.* Nevertheless, such parameters are fixed by the user afterwards and all the spaced seeds which do not meet such requisites will be automatically ignored by the program.

Here is a simple example of spaced seeds definition.
```
>T295
1111011101110010111001011011111
>T58570
1111101001110101101100111011111
# This is a comment line
>T38570
1111101011100101101110011011111
```
Each spaced seed entry should start with a `>` char directly followed by the name of the seed. Please do not enter blank spaces.

In the line immediately below the spaced seed itself must appear as a bynary string carrying `1` characters for "match" and `0` for "don't care". **Each spaced seed should both start and end with a `1` character and must not exceed 32 characters of length. Futhermore, no comment lines are allowed between the name line and the seed line.**

A line starting with a `#` is ignored, this can be used for comments, but please use comments only in between different spaced seeds (i.e., before a line starting with `>`).

Please stick stirctly to this format as the reading of the file is very naive.

### Spaced database building

As in the original CLARK-S version, the building of the database of discriminative spaced k-mers of the desired targets require some steps. Please refer to the original README.txt file (at line 640) provided by CLARK developers.
1. Run `set_targets.sh` for target definition.
2. Build the database of discriminative k-mers, where k must be the desired length of the spaced seeds. (You can run classify_metagenome.sh as suggested by CLARK authors).
3. At this point, instead of running `buildSpacedDB.sh`, you should run `buildMutableSpacedDB.sh` followed by the length of the spaced seeds, their wigth and the filename of the file where you have defined your personalized set of seeds. Here is an example.
```
./buildMutableSpacedDB.sh 31 22 ./my_spaced_seeds.txt
```
At this point the spaced database should be created.

### Running CLARK-S with personalized spaced seeds
Now you can run CLARK-S, but remeber you must specify the spaced seeds file you used for the database creation by means of `-S` option (notice it is a capital S), the k-mer/spaced seeds length `-k` the seeds weight `-w` (if different from the default `-k 31 -w 22`).
An example of execution command is the following:
```
$ ./CLARK-S -k 31 -T ./targets_addresses.txt -D ./DBD/ -O ./objects.fa -R ./results -S my_spaced_seeds.txt -w 22 -m 0 -i 2
```
