/*
	Name:	SpacedSeedsManager
	Author:	Giorgio Gallina
	Date:	26 October 2021
	Description: This class is used for reading spaced seeds from file,
	             memorizing and managing them.
*/

#ifndef SPACED_SEEDS_MANAGER_HH
#define SPACED_SEEDS_MANAGER_HH

#include "./MISSH/MultiSpacedQmer.hh"
#include "./MISSH/SpacedQmer_Multi.hh"
#include "./kmersConversion.hh"
#include <iostream>
#include <fstream>
#include <bits/stdc++.h>
#include <map>
#include <cstring>
#include <vector>
using namespace std;

struct SpSeedElement{
	
	private:
	static const size_t MAX_SEED_LEN = 32;
	bool   is_proper_seed;
	public:
	string fwd01;	// forward spaced seed in 01 format
	string rvs01;	// reverse spaced seed in 01 format
	int8_t indx;	// index of the seed (useless actally)
	size_t weight;	// number of '1' chars in the seed
	size_t length;
	
	// structures for native CLARK-S hash computation
	uint8_t shifts_block_count[64];	// counts of chars for each block of contig equal chars
	uint8_t shifts_cumulative_count[64]; // array of "positions" based on previous counts
	uint8_t shifts_size; // #blocks of contig equal chars
	
	public:
	SpSeedElement()
	{	clear(); }
	
	SpSeedElement(const string &spaced_seed, size_t fixed_length = 0, size_t fixed_weight = 0, int8_t index = 0)
	{
		string spseed = spaced_seed;
		
		is_proper_seed = true;
		
		// check if spseed starts with '1'. If not, ignore all chars preceding the first '1'.
		if(spseed[0] != '1')
		{	cerr << "> First char of a seed is supposed to be '1'. Found '" << spseed[0] << "', instead." << endl;
			cerr << "  First wrong chars ignored." << endl;
			spseed = string(spseed, spseed.find('1'));
			//is_proper_seed = false;
		}
		
		length = (size_t) spseed.length();
		
		// check if tractable length and
		if(length > MAX_SEED_LEN)
		{	cerr << "> Intractable spaced seed due to its length > " << MAX_SEED_LEN << ". Length: " << length << endl;
			cerr << "  Spaced seed truncated to " << MAX_SEED_LEN << " chars." << endl;
			length = MAX_SEED_LEN;
			is_proper_seed = false;
		}
		
		// check if fixed length is required
		if(fixed_length != 0)
		{	
			if(length > fixed_length)
			{
				cerr << "> Spaced seed exceed the desired length > " << fixed_length << ". Length: " << length << endl;
				cerr << "  Spaced seed truncated to " << fixed_length << " chars." << endl;
				length = fixed_length;
				is_proper_seed = false;
			}else if(length < fixed_length)
			{
				cerr << "> Spaced seed of length inferior to the desired one. Length: " << length << " < " << fixed_length << endl;
				is_proper_seed = false;
			}
		}
		
		// check if spseed finishes with '1'
		if(spseed[length-1] != '1')
		{	cerr << "> Last char of a seed is supposed to be '1'. Found '" << spseed[length-1]  << "' (" << (unsigned int) spseed[length-1] << "), instead." << endl;
			// TODO: maybe shrink to fit
			is_proper_seed = false;
		}
		
		
		fwd01 = string(spseed, 0, length);
		
		rvs01 = string(fwd01);
		reverse(rvs01.begin(), rvs01.end());
		
		indx  = index;
		
		weight = (size_t) count(fwd01.begin(), fwd01.end(), '1');
		if(fixed_weight != 0 && weight != fixed_weight)
		{	cerr << "> Weight different from the desired one. Weight = " << weight << " != " << fixed_weight << endl;
			is_proper_seed = false;
		}
		
		
		// check if spseed is well formed (only '0's and '1's)
		if(length != (weight + (uint8_t)count(fwd01.begin(), fwd01.end(), '0')))
		{	cerr << "> Unknown spaced seed format. Please correct your file." << endl;
			cerr << "  Expected binary string ('0' and '1' characters). Found, instead:" << endl;
			cerr << "  " << fwd01 << endl;
			is_proper_seed = false;
			//exit(1);
		}
		
		/* ---------------  Setting structures for CLARK-S hash computation  --------------- */
		uint8_t current_count = 1; //the first char of a spaced seed is always '1'
		// #elements stored
		shifts_size = 0;
		// count contiguous equal chars in the seed
		for(uint8_t i = 1; i < length; i++)
		{
			// increment count if the block has not yet been read completely
			if(fwd01[i] == fwd01[i-1])
			{	current_count++; }
			else	// block end: store number of element in it
			{	shifts_block_count[shifts_size] = current_count << 1;
				shifts_size++;
				// start counting elements of the new block
				current_count = 1;
			}
		}
		shifts_block_count[shifts_size++] = current_count << 1; //last block of '1's

		// compute positions for shifts
		//it is important to set the last element of shifts_cumulative_count to 0 because it is used (even if not considered element of the array)
		shifts_cumulative_count[shifts_size] = current_count = 0; 
		for(int8_t i = shifts_size-1; i >= 0; i--)
		{
			current_count += shifts_block_count[i];
			shifts_cumulative_count[i] = current_count;
		}
		/*
		cerr << ">>shifts_block_count" << endl;
		for(uint8_t i = 0; i < shifts_size; i++)
		{	cerr << "  " << (int) shifts_block_count[i]; }
		cerr << endl << ">>shifts_cumulative_count" << endl;
		for(uint8_t i = 0; i < shifts_size; i++)
		{	cerr << "  " << (int) shifts_cumulative_count[i]; }*/
	}
	
	bool is_usable()
	{	return is_proper_seed; }
	
	void clear()
	{	
		this->indx = -1;
		this->weight = this->length = 0;
		this->shifts_size = 0;
		this->is_proper_seed = false;
	}
};


class SpacedSeedsManager{
	
	private:
	// map of spaced seeds elements indexed by their name (ie settings)
	inline static map<string, SpSeedElement> spSeeds_table;
	// number of elements in the map
	inline static size_t size;
	
	public:
	// vectorn of spaced seeds names (settings)
	inline static vector<string> name_list;
	
	
	SpacedSeedsManager()
	{	clear(); }
	
	
	/*  ---------------   default CLARK-S seeds   ---------------  */
	static bool init()
	{
		clear();
		
		cerr << "======= Processing T295  =========================== " << endl; 
		spSeeds_table["T295"]   = SpSeedElement("1111011101110010111001011011111", 0, 0, 0);
		cerr << "# Done." << endl;
		cerr << "==================================================== " << endl << endl;
		
		cerr << "======= Processing T58570  ========================= " << endl; 
		spSeeds_table["T58570"] = SpSeedElement("1111101001110101101100111011111", 0, 0, 1);
		cerr << "# Done." << endl;
		cerr << "==================================================== " << endl << endl;
		
		cerr << "======= Processing T38570  ========================= " << endl; 
		spSeeds_table["T38570"] = SpSeedElement("1111101011100101101110011011111", 0, 0, 2);
		cerr << "# Done." << endl;
		cerr << "==================================================== " << endl << endl;
		
		size = (size_t) 3;
		
		name_list.push_back("T295");
		name_list.push_back("T58570");
		name_list.push_back("T38570");
		
		/* for(map<string,SpacedSeedElement>::iterator it = spSeeds_table.begin(); it != spSeeds_table.end(); it++)
		{
			name_list.push_back(it->first);
		} */

		cerr << string((90) , '#') << endl << endl;

		return true;
	}
	
	
	/*  ---------------   read spaced seeds from file   ---------------  */
	static bool init(const string& filename, size_t fixed_length = 0, size_t fixed_weight = 0)
	{
		clear();

		cerr << string((90) , '#') << endl;

		// open spaced seeds file
		ifstream filein(filename, ios::in);
		if(filein.is_open())
		{
			char buffer[256];
			string ss_name, ss_string;
			
			// read the file
			while(!filein.eof())
			{
				if(filein.getline(buffer, 256))
				{
					// -----  IGNORE COMMENT LINE  ----- //
					if(buffer[0] == '#')
					{	continue; }

					// ----- PROCESSING SPACED SEED ----- //
					cerr << "======= Processing spaced seed: \"" << &buffer[1] << "\"  ========================= " << endl; 
					//TODO (useless right now): check correctness of format: is (buffer[0] == '>') ?
					if(buffer[0] != '>')
					{	cerr << ">>Spaced seeds file badly formed. Expected: \">Spaced_Seed_Name\"" << endl;
						cerr << "  Found:\"" << buffer << "\", instead." << endl;
					}
					
					// get spaced seed name (setting)
					ss_name = string(&buffer[1]);
					
					// read spaced seed in the next line
					if(filein.getline(buffer, 256))
					{
						ss_string = string(buffer);
						// create spaced seed structure based on the line read and store it in the map
						spSeeds_table[ss_name] = SpSeedElement(ss_string, fixed_length, fixed_weight);
						size++;

						if(spSeeds_table[ss_name].is_usable())
						{	cerr << "#  Done." << endl; }					
					}else
					{	cerr << ">  Error in file reading." << endl; }
					
					cerr << string( (61+ss_name.length()) , '=' ) << endl << endl;
				}
			}
			
			filein.close();
			
			cerr << "SELECTING Spaced Seeds" << endl;
			// store "orderedly" names of USABLE spaced seeds, remove improper spaced seeds.
			map<string,SpSeedElement>::iterator it = spSeeds_table.begin();
			while( it != spSeeds_table.end() )
			{
				cerr << "   [" << it->first;
				if(it->second.is_usable())
				{ 	name_list.push_back(it->first);
					cerr << "] selected." << endl;
					it++;
				}else
				{	map<string,SpSeedElement>::iterator itr = it;
					it++;
					spSeeds_table.erase(itr);
					size--;
					cerr << "] ignored." << endl;
				}
			}
		}else
		{
			cerr << "Failed to open file " << filename << endl;
			//return false;
		}
		
		// if something went wrong (no spaced seeds found), then use default spaced seeds
		if(size == 0)
		{
			cerr << "Failed to load spaced seeds. Using default ones instead." << endl;
			return init(); // TODO: or return false?
		}

		cerr << string((90) , '#') << endl << endl;
		
		return true;
	}
	
	static bool init(const char* filename, size_t fixed_length = 0, size_t fixed_weight = 0)
	{
		return init(string(filename), fixed_length, fixed_weight);
	}
	
	
	static vector<string> getSpacedSeedsNames()
	{	return name_list; }
	
	
	static size_t getSize()
	{	return size; }
	
	
	static string getSpacedSeedString(const string& seed_name)
	{
		return spSeeds_table.at(seed_name).fwd01;
	}
	
	
	static string getSpacedSeedReverseString(const string& seed_name)
	{
		return spSeeds_table.at(seed_name).rvs01;
	}
	
	static string getSpacedSeedString(const uint8_t& seed_index)
	{
		return spSeeds_table.at(name_list[seed_index]).fwd01;
	}
	
	
	static string getSpacedSeedReverseString(const uint8_t& seed_index)
	{
		return spSeeds_table.at(name_list[seed_index]).rvs01;
	}
	
	static size_t getSpacedSeedWeight(const uint8_t& seed_index)
	{
		return spSeeds_table.at(name_list[seed_index]).weight;
	}
	
	static bool getSpacedKmer(const string& spaced_seed_name, const uint64_t& kmerR, uint64_t& skmerR)
	{
		SpSeedElement seed;
		
		// get spaced seed element
		try
		{	seed = spSeeds_table.at(spaced_seed_name); }
		catch(out_of_range oor)
		{	return false; } // unknown spaced seed
		
		// init. skmerR with the first block of countiguous chars selected from kmerR
		skmerR = kmerR >> seed.shifts_cumulative_count[1];
		
		// compute spaced kmer (see kmersConversion.cc for the computation with hard-coded spaced seeds)
		for(uint8_t i = 2; i < seed.shifts_size; i += 2)
		{
			// make space for the new chars to add
			skmerR <<= seed.shifts_block_count[i];
			// create mask to ignore chars preceding the ones to encode at this iteration
			uint64_t mask = ((uint64_t)-1) >> (64 - seed.shifts_cumulative_count[i]);
			// select the current block of contig chars from kmerR and copy to skmerR
			skmerR ^= (kmerR & mask) >> seed.shifts_cumulative_count[i+1];
		}
		return true;
	}
	
	
	static void clear()
	{
		size = 0;
		spSeeds_table.clear();
		name_list.clear();
	}
};


//size_t SpacedSeedsManager::size = 0;


#endif
