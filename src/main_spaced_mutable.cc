/*
 * CLARK, CLAssifier based on Reduced K-mers.
 */

/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Copyright 2013-2019, Rachid Ounit <clark.ucr.help at gmail.com>
 */
 
 /*
 * @author: Giorgio Gallina (modification of Rachid Ounit, Ph.D Candidat, code)
 * @project: Integration of MISSH on CLARK-S & management of personalized spaced seeds
 * @note: none.
 *
 */

#include <cstring>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include "./kmersConversion.hh"
#include "./contiguousToSpaced_hh.hh"
#include "./parameters.hh"
#include "./SpacedSeedsManager.hh"

using namespace std;

int main(int argc, const char** argv)
{
	if (argc != 6)
	{
		cerr << argv[0] << " <DatabaseFilename: TSK file> <weight> <len> <setting:T295,...> <spaced_seeds_filename>"<< endl;
		return 1;
	}
	size_t w  = atoi(argv[2]);
	size_t len = atoi(argv[3]);
	string setting(argv[4]);
	size_t t_b = log(HTSIZE)/log(4.0);
        size_t max16 = t_b + 8;
        size_t max32 = t_b + 16;
        string sps_fname = argv[5];
        
        SpacedSeedsManager::init(sps_fname, len, w);

	string file(argv[1]);
	string sfile = file.substr(0,file.size()-3);
	if (max16 >= w)
	{
		contiguousTospaced<T32,T16> converter(w, setting, len);
		converter.populate(sfile.c_str());
		converter.write(sfile.c_str());
		exit(0);
	}
	if (max32 >= w)
        {
                contiguousTospaced<T32,T32> converter(w, setting, len);
                converter.populate(sfile.c_str());
                converter.write(sfile.c_str());
                exit(0);
        }
	return 1;
}

