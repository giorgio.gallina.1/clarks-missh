/*
	Name: MISSH_Shifts
	Author: Giorgio Gallina
	Date: 04/08/20 14:15
	Description: This class provides basics data structures and functions to calculate and access the
		table containing information about the previous calculated hash to use in MISSH algorithm.
*/

#ifndef MISSH_SHIFTS_HH
#define MISSH_SHIFTS_HH

#include "./MISSH/MultiSpacedQmer.hh"
#include "./MISSH/SpacedQmer_Multi.hh"
#include "./kmersConversion.hh"
#include "./SpacedSeedsManager.hh"
#include <iostream>
#include <fstream>
#include <bits/stdc++.h>
using namespace std;

// test mode:
	//	0 = CLARK fwr, rvs
	//	1 = MISSH fwr, CLARK rvs
	//	2 = MISSH fwr, rvs
static uint8_t MISSH_reverse_mode;

class MISSH_Shifts{
	
	private:
	// table of previous shifts
	MultiSeedInfo info_col_shifts;
	// #spaced-seeds considered
	size_t size;
	// length of each spaced seed
	size_t* seeds_size;
	// weight of spaced seed
	size_t* seeds_weight;
		// file location
		const string db_path = "../misshfile/shifts.bin";
	
	
	public:
	MISSH_Shifts()
	{	clear(); }
	
	/*  Default initialization function. The 3 spaced seeds original of CLARK-S are considered.  */
	bool init()
	{
		// mode 0 = CLARK-S without MISSH: no work to do
		if(MISSH_reverse_mode == 0)
		{	return false; }
	
		// number of spaced seeds
		size = 3;
		// size of each spaced seed
		seeds_size = new size_t[3];
		seeds_size[0] = seeds_size[1] = seeds_size[2] = 31;
		seeds_weight = new size_t[3];
		seeds_weight[0] = seeds_weight[1] = seeds_weight[2] = 22;
		/*  NOTE: in MISSH_reverse_mode==2, for each seed, its reverse is stored as weel.
		 *  Therefore, the size of the set is doubled, but the number of spaced seeds to
		 *  use remains unchanged. Also, the size of each seed added equals the size of
		 *  its reverse.
		 */
		 
		// dimensione array seeds
		size_t real_size = (MISSH_reverse_mode == 2) ? (size << 1) : size;
		
			// loading info_col_shifts from memory if found
			//bool io_flag = load();
	
			// if loading was not possible, initialize info_col_shifts (and store)
			//if(!io_flag)
			{
			// vector of spaced seeds
			vector<SpacedQmer> seeds;//(real_size);
			size_t numprev = 0;
			
			seeds.push_back(SpacedQmer("1111011101110010111001011011111", numprev));	//OPTSS95s2 - T295
			seeds.push_back(SpacedQmer("1111101001110101101100111011111", numprev));	//T58570
			seeds.push_back(SpacedQmer("1111101011100101101110011011111", numprev));	//T38570
			
			if(MISSH_reverse_mode == 2)	//reverse complement
			{
				seeds.push_back(SpacedQmer("1111101101001110100111011101111", numprev));	//OPTSS95s2 - T295
				seeds.push_back(SpacedQmer("1111101110011011010111001011111", numprev));	//T58570
				seeds.push_back(SpacedQmer("1111101100111011010011101011111", numprev));	//T38570
			}
				
			// shifts structure initializing
			MultiSpacedQmer MultiSeed(seeds);
			info_col_shifts = MultiSeed.Get_multi_seed_info_col();
			
				// storing shifts
				//save();
			}
		
		return true;
		
	}
	
	/* Initialization given spaced seeds name. DEPRECATED */
	bool init(const vector<string>& seeds_str, size_t numprev = 0)
	{
		if(MISSH_reverse_mode == 0)
		{	return false; }
		
		clear();		
		
		// number of spaced seeds
		size = (size_t) seeds_str.size();
		// size of each spaced seed
		seeds_size = new size_t[size];
		seeds_weight = new size_t[size];
		
		/*  NOTE: in MISSH_reverse_mode==2, for each seed, its reverse is stored as weel.
		 *  Therefore, the size of the set is doubled, but the number of spaced seeds to
		 *  use remains unchanged. Also, the size of each seed added equals the size of
		 *  its reverse.
		 */
		
		// dimensione array seeds
		size_t real_size = (MISSH_reverse_mode == 2) ? (size << 1) : size;
		// vector of spaced seeds
		vector<SpacedQmer> seeds;//(real_size);
		
		// insert the spaced seeds
		for(size_t i = 0; i < size; i++)
		{
			string spseed_tmp = getSpacedSeedString(string(seeds_str[i]));
			seeds.push_back(SpacedQmer(spseed_tmp, numprev));
			size_t curr_size = (size_t) spseed_tmp.size();
			seeds_size[i] = curr_size;
			// peso weight = #'1'
			size_t curr_weight = 0;
			for(uint8_t j = 0; j < curr_size; j++)
			{	if(spseed_tmp[j] == '1')
				{	curr_weight++; }
			}
			seeds_weight[i] = curr_weight;
		}
		
		if(MISSH_reverse_mode == 2)
		{
			// insert spaced seeds reversed
			string tmp;
			for(size_t i = 0; i < size; i++)
			{
				tmp = getSpacedSeedString(string(seeds_str[i]));
				reverse(tmp.begin(), tmp.end());
				seeds.push_back(SpacedQmer(tmp, numprev));
			}
		}
		
			//SpacedQmer_Multi seeds_info;
			//seeds_info.init(seeds);
			
		// shifts structure initializing
		MultiSpacedQmer MultiSeed(seeds);
		info_col_shifts = MultiSeed.Get_multi_seed_info_col();
		
			// storing shifts
			//save();
			
		return true;
	}
	
	/* Initialization given spaced seeds in their proper manager object */
	bool init(bool use_default_seeds, size_t numprev = 0) //bool init(SpacedSeedsManager& spaced_seeds, size_t numprev = 0)
	{
		if(MISSH_reverse_mode == 0)
		{	return false; }
		
		if(use_default_seeds)
		{	return init(); }
		
		clear();
		
		// number of spaced seeds
		size = (size_t) SpacedSeedsManager::getSize();//spaced_seeds.getSize();
		// size of each spaced seed
		seeds_size = new size_t[size];
		seeds_weight = new size_t[size];
		
		/*  NOTE: in MISSH_reverse_mode==2, for each seed, its reverse is stored as weel.
		 *  Therefore, the size of the set is doubled, but the number of spaced seeds to
		 *  use remains unchanged. Also, the size of each seed added equals the size of
		 *  its reverse.
		 */
		
		// dimensione array seeds
		size_t real_size = (MISSH_reverse_mode == 2) ? (size << 1) : size;
		// vector of spaced seeds
		vector<SpacedQmer> seeds;
		seeds.reserve(real_size);
		// tmp vector for reverse seeds
		vector<SpacedQmer> seeds_rvs_tmp;
		seeds_rvs_tmp.reserve(size);
		
		// insert the spaced seeds
		for(uint8_t i = 0; i < size; i++)
		{
			string spseed_tmp = SpacedSeedsManager::getSpacedSeedString(i); //spaced_seeds.getSpacedSeedString(i);
			string spseed_rvs_tmp = SpacedSeedsManager::getSpacedSeedReverseString(i); //spaced_seeds.getSpacedSeedReverseString(i);
			
			seeds.push_back(SpacedQmer(spseed_tmp, numprev));
			if(MISSH_reverse_mode == 2)
			{	seeds_rvs_tmp.push_back(SpacedQmer(spseed_rvs_tmp, numprev));}
			
			seeds_size[i] = (size_t) spseed_tmp.size();
			seeds_weight[i] = SpacedSeedsManager::getSpacedSeedWeight(i); //spaced_seeds.getSpacedSeedWeight(i);
		}
		// insert reverse seeds
		for(uint8_t i = 0; i < seeds_rvs_tmp.size(); i++)
		{
			seeds.push_back(seeds_rvs_tmp[i]);
		}
			
		// shifts structure initializing
		MultiSpacedQmer MultiSeed(seeds);
		info_col_shifts = MultiSeed.Get_multi_seed_info_col();
		
		return true;
	}
	
	MultiSeedInfo getInfoColShifts()
	{	return info_col_shifts; }
	
	size_t getSize()
	{	return size; }
	
	size_t* getSeedsSize()
	{	return seeds_size; }
	
	size_t* getSeedsWeight()
	{	return seeds_weight; }
	
	size_t getRealSize()    //WARNING: non modificare i codici di MISSH_reverse_mode senza aggiustare questa function
	{	return ((size_t)MISSH_reverse_mode)*size; }
	
	
	
	
	
	
	private:
	bool load()	//NOT TESTED
	{
		ifstream filein(db_path, ios::binary | ios::in);
		if(filein.is_open())
		{
			filein.read((char*)&info_col_shifts, sizeof(MultiSeedInfo));
			if(!filein)
			{	
				info_col_shifts.clear();
				filein.close();
				return false;
			}
			
			filein.read((char*)&size, sizeof(size_t));
			if(!filein)
			{	
				info_col_shifts.clear();
				filein.close();
				return false;
			}
			
			filein.read((char*)seeds_size, sizeof(size_t)*size);
			if(!filein)
			{	
				info_col_shifts.clear();
				filein.close();
				return false;
			}
			
			filein.read((char*)seeds_weight, sizeof(size_t)*size);
			if(!filein)
			{	
				info_col_shifts.clear();
				filein.close();
				return false;
			}
			
			filein.close();
			return true;
		}
		return false;
	}
	
	bool save()	//NOT TESTED
	{
		ofstream outf(db_path, ios::binary | ios::in);
		if(outf.is_open())
		{
			outf.write((char*)&info_col_shifts, sizeof(MultiSeedInfo));
			if(!outf)
			{
				outf.close();
				return false;
			}
			
			outf.write((char*)&size, sizeof(size_t));
			if(!outf)
			{
				outf.close();
				return false;
			}
			
			outf.write((char*)seeds_size, size*sizeof(size_t));
			if(!outf)
			{
				outf.close();
				return false;
			}
			
			outf.write((char*)seeds_weight, size*sizeof(size_t));
			if(!outf)
			{
				outf.close();
				return false;
			}
			
			outf.close();
			return true;
		}
		return false;
	}
	
	public:
	void clear()	//NOT TESTED
	{
		size = 0;
		info_col_shifts.clear();
		delete seeds_size;
		delete seeds_weight;
	}
};


#endif
