/*
 * @author: Giorgio Gallina
 * @project: Integration of MISSH on CLARK-S & management of personalized spaced seeds
 * @note: none.
 *
 */

#include <cstring>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "./parameters.hh"
#include "./SpacedSeedsManager.hh"

using namespace std;

int main(int argc, const char** argv)
{
	/*	// useless control since not manageble by user
		if (argc != 4)
		{
			cerr << argv[0] << " <k> <w> <spaceSeeds_file_path>" << endl;
			return 1;
		}
	*/
	
	unsigned int k = (unsigned int) atoi(argv[1]);
	unsigned int w = (unsigned int) atoi(argv[2]);
	const char* sps_fname = argv[3];
	char bash_name[128];

	sprintf(bash_name, "./.buildModifiedSpDB_k%uw%u.bash", k, w);
	
	ofstream bash_writer(bash_name);
	
	if(bash_writer.is_open())
	{
		bash_writer << "#!/bin/bash" << endl << endl << endl;
		
		
		bash_writer << "RPT=$(readlink -f \"$0\")" << endl;
		bash_writer << "LDIR=$(dirname \"$FSCRPT\")" << endl << endl;

		bash_writer << "DIR=\"\"" << endl;
		bash_writer << "for db in `cat $LDIR/.dbAddress`" << endl;
		bash_writer << "do" << endl;
		bash_writer << "DIR=\"$db\"" << endl;
		bash_writer << "done" << endl << endl << endl;
		
		
		// read spaced seeds.
		if(SpacedSeedsManager::init(sps_fname, k, w))
		{
			string setting;
			for(size_t i = 0; i < SpacedSeedsManager::getSize(); i++)
			{
				setting = SpacedSeedsManager::name_list[i];
				
				bash_writer << "if [ -s $DIR/" << setting << "/db_central_k" << k << "_t*_s1610612741_m0_w" << w << ".tsk.sz ]; then" << endl;
				bash_writer << "echo \"Database for the spaced seed (" << setting << ") already exists.\"" << endl;
				bash_writer << "else" << endl;
				bash_writer << "echo \"Building database " << setting << "\"" << endl;
				bash_writer << ">&2 echo \"Building database " << setting << "\"" << endl;
				bash_writer << "$LDIR/exe/.converterMutable $DIR/db_central_k" << k << "*m0.tsk.sz " << w << " " << k << " " << setting << " " << sps_fname << endl;
				bash_writer << "if [ ! -d $DIR/" << setting <<" ]; then " << endl;
				bash_writer << "\tmkdir $DIR/" << setting << "/" << endl;
				bash_writer << "fi" << endl;
				bash_writer << "mv $DIR/db_central_k" << k << "_t*_s1610612741_m0_w" << w << ".tsk.* $DIR/" << setting << "/" << endl;
				bash_writer << "fi" << endl << endl << endl;
			}
		}
		
		bash_writer.close();
	}else
	{	
		cerr << "Could not open file: " << bash_name << endl;
		return 1;
	}
	
	
	
	
	return 1;
}

